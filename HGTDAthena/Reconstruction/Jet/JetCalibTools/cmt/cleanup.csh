# echo "cleanup JetCalibTools JetCalibTools-00-04-64 in /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/Reconstruction/Jet"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.7/CMT/v1r25p20160527
endif
source ${CMTROOT}/mgr/setup.csh
set cmtJetCalibToolstempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtJetCalibToolstempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=JetCalibTools -version=JetCalibTools-00-04-64 -path=/export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/Reconstruction/Jet  $* >${cmtJetCalibToolstempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=JetCalibTools -version=JetCalibTools-00-04-64 -path=/export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/Reconstruction/Jet  $* >${cmtJetCalibToolstempfile}"
  set cmtcleanupstatus=2
  /bin/rm -f ${cmtJetCalibToolstempfile}
  unset cmtJetCalibToolstempfile
  exit $cmtcleanupstatus
endif
set cmtcleanupstatus=0
source ${cmtJetCalibToolstempfile}
if ( $status != 0 ) then
  set cmtcleanupstatus=2
endif
/bin/rm -f ${cmtJetCalibToolstempfile}
unset cmtJetCalibToolstempfile
exit $cmtcleanupstatus

