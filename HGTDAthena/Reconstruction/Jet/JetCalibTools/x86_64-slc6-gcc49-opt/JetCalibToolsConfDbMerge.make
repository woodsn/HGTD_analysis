#-- start of make_header -----------------

#====================================
#  Document JetCalibToolsConfDbMerge
#
#   Generated Wed Jun 21 12:07:11 2017  by woodsn
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_JetCalibToolsConfDbMerge_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_JetCalibToolsConfDbMerge_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_JetCalibToolsConfDbMerge

JetCalibTools_tag = $(tag)

#cmt_local_tagfile_JetCalibToolsConfDbMerge = $(JetCalibTools_tag)_JetCalibToolsConfDbMerge.make
cmt_local_tagfile_JetCalibToolsConfDbMerge = $(bin)$(JetCalibTools_tag)_JetCalibToolsConfDbMerge.make

else

tags      = $(tag),$(CMTEXTRATAGS)

JetCalibTools_tag = $(tag)

#cmt_local_tagfile_JetCalibToolsConfDbMerge = $(JetCalibTools_tag).make
cmt_local_tagfile_JetCalibToolsConfDbMerge = $(bin)$(JetCalibTools_tag).make

endif

include $(cmt_local_tagfile_JetCalibToolsConfDbMerge)
#-include $(cmt_local_tagfile_JetCalibToolsConfDbMerge)

ifdef cmt_JetCalibToolsConfDbMerge_has_target_tag

cmt_final_setup_JetCalibToolsConfDbMerge = $(bin)setup_JetCalibToolsConfDbMerge.make
cmt_dependencies_in_JetCalibToolsConfDbMerge = $(bin)dependencies_JetCalibToolsConfDbMerge.in
#cmt_final_setup_JetCalibToolsConfDbMerge = $(bin)JetCalibTools_JetCalibToolsConfDbMergesetup.make
cmt_local_JetCalibToolsConfDbMerge_makefile = $(bin)JetCalibToolsConfDbMerge.make

else

cmt_final_setup_JetCalibToolsConfDbMerge = $(bin)setup.make
cmt_dependencies_in_JetCalibToolsConfDbMerge = $(bin)dependencies.in
#cmt_final_setup_JetCalibToolsConfDbMerge = $(bin)JetCalibToolssetup.make
cmt_local_JetCalibToolsConfDbMerge_makefile = $(bin)JetCalibToolsConfDbMerge.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)JetCalibToolssetup.make

#JetCalibToolsConfDbMerge :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'JetCalibToolsConfDbMerge'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = JetCalibToolsConfDbMerge/
#JetCalibToolsConfDbMerge::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/merge_genconfDb_header
# Author: Sebastien Binet (binet@cern.ch)

# Makefile fragment to merge a <library>.confdb file into a single
# <project>.confdb file in the (lib) install area

.PHONY: JetCalibToolsConfDbMerge JetCalibToolsConfDbMergeclean

# default is already '#'
#genconfDb_comment_char := "'#'"

instdir      := ${CMTINSTALLAREA}/$(tag)
confDbRef    := /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/Reconstruction/Jet/JetCalibTools/genConf/JetCalibTools/JetCalibTools.confdb
stampConfDb  := $(confDbRef).stamp
mergedConfDb := $(instdir)/lib/$(project).confdb

JetCalibToolsConfDbMerge :: $(stampConfDb) $(mergedConfDb)
	@:

.NOTPARALLEL : $(stampConfDb) $(mergedConfDb)

$(stampConfDb) $(mergedConfDb) :: $(confDbRef)
	@echo "Running merge_genconfDb  JetCalibToolsConfDbMerge"
	$(merge_genconfDb_cmd) \
          --do-merge \
          --input-file $(confDbRef) \
          --merged-file $(mergedConfDb)

JetCalibToolsConfDbMergeclean ::
	$(cleanup_silent) $(merge_genconfDb_cmd) \
          --un-merge \
          --input-file $(confDbRef) \
          --merged-file $(mergedConfDb)	;
	$(cleanup_silent) $(remove_command) $(stampConfDb)
libJetCalibTools_so_dependencies = ../x86_64-slc6-gcc49-opt/libJetCalibTools.so
#-- start of cleanup_header --------------

clean :: JetCalibToolsConfDbMergeclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(JetCalibToolsConfDbMerge.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

JetCalibToolsConfDbMergeclean ::
#-- end of cleanup_header ---------------
