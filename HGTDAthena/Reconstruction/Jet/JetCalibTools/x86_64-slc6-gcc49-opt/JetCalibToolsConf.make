#-- start of make_header -----------------

#====================================
#  Document JetCalibToolsConf
#
#   Generated Wed Jun 21 12:07:07 2017  by woodsn
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_JetCalibToolsConf_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_JetCalibToolsConf_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_JetCalibToolsConf

JetCalibTools_tag = $(tag)

#cmt_local_tagfile_JetCalibToolsConf = $(JetCalibTools_tag)_JetCalibToolsConf.make
cmt_local_tagfile_JetCalibToolsConf = $(bin)$(JetCalibTools_tag)_JetCalibToolsConf.make

else

tags      = $(tag),$(CMTEXTRATAGS)

JetCalibTools_tag = $(tag)

#cmt_local_tagfile_JetCalibToolsConf = $(JetCalibTools_tag).make
cmt_local_tagfile_JetCalibToolsConf = $(bin)$(JetCalibTools_tag).make

endif

include $(cmt_local_tagfile_JetCalibToolsConf)
#-include $(cmt_local_tagfile_JetCalibToolsConf)

ifdef cmt_JetCalibToolsConf_has_target_tag

cmt_final_setup_JetCalibToolsConf = $(bin)setup_JetCalibToolsConf.make
cmt_dependencies_in_JetCalibToolsConf = $(bin)dependencies_JetCalibToolsConf.in
#cmt_final_setup_JetCalibToolsConf = $(bin)JetCalibTools_JetCalibToolsConfsetup.make
cmt_local_JetCalibToolsConf_makefile = $(bin)JetCalibToolsConf.make

else

cmt_final_setup_JetCalibToolsConf = $(bin)setup.make
cmt_dependencies_in_JetCalibToolsConf = $(bin)dependencies.in
#cmt_final_setup_JetCalibToolsConf = $(bin)JetCalibToolssetup.make
cmt_local_JetCalibToolsConf_makefile = $(bin)JetCalibToolsConf.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)JetCalibToolssetup.make

#JetCalibToolsConf :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'JetCalibToolsConf'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = JetCalibToolsConf/
#JetCalibToolsConf::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/genconfig_header
# Author: Wim Lavrijsen (WLavrijsen@lbl.gov)

# Use genconf.exe to create configurables python modules, then have the
# normal python install procedure take over.

.PHONY: JetCalibToolsConf JetCalibToolsConfclean

confpy  := JetCalibToolsConf.py
conflib := $(bin)$(library_prefix)JetCalibTools.$(shlibsuffix)
confdb  := JetCalibTools.confdb
instdir := $(CMTINSTALLAREA)$(shared_install_subdir)/python/$(package)
product := $(instdir)/$(confpy)
initpy  := $(instdir)/__init__.py

ifdef GENCONF_ECHO
genconf_silent =
else
genconf_silent = $(silent)
endif

JetCalibToolsConf :: JetCalibToolsConfinstall

install :: JetCalibToolsConfinstall

JetCalibToolsConfinstall : /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/Reconstruction/Jet/JetCalibTools/genConf/JetCalibTools/$(confpy)
	@echo "Installing /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/Reconstruction/Jet/JetCalibTools/genConf/JetCalibTools in /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/InstallArea/python" ; \
	 $(install_command) --exclude="*.py?" --exclude="__init__.py" --exclude="*.confdb" /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/Reconstruction/Jet/JetCalibTools/genConf/JetCalibTools /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/InstallArea/python ; \

/export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/Reconstruction/Jet/JetCalibTools/genConf/JetCalibTools/$(confpy) : $(conflib) /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/Reconstruction/Jet/JetCalibTools/genConf/JetCalibTools
	$(genconf_silent) $(genconfig_cmd)   -o /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/Reconstruction/Jet/JetCalibTools/genConf/JetCalibTools -p $(package) \
	  --configurable-module=GaudiKernel.Proxy \
	  --configurable-default-name=Configurable.DefaultName \
	  --configurable-algorithm=ConfigurableAlgorithm \
	  --configurable-algtool=ConfigurableAlgTool \
	  --configurable-auditor=ConfigurableAuditor \
          --configurable-service=ConfigurableService \
	  -i ../$(tag)/$(library_prefix)JetCalibTools.$(shlibsuffix)

/export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/Reconstruction/Jet/JetCalibTools/genConf/JetCalibTools:
	@ if [ ! -d /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/Reconstruction/Jet/JetCalibTools/genConf/JetCalibTools ] ; then mkdir -p /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/Reconstruction/Jet/JetCalibTools/genConf/JetCalibTools ; fi ;

JetCalibToolsConfclean :: JetCalibToolsConfuninstall
	$(cleanup_silent) $(remove_command) /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/Reconstruction/Jet/JetCalibTools/genConf/JetCalibTools/$(confpy) /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/Reconstruction/Jet/JetCalibTools/genConf/JetCalibTools/$(confdb)

uninstall :: JetCalibToolsConfuninstall

JetCalibToolsConfuninstall ::
	@$(uninstall_command) /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/InstallArea/python
libJetCalibTools_so_dependencies = ../x86_64-slc6-gcc49-opt/libJetCalibTools.so
#-- start of cleanup_header --------------

clean :: JetCalibToolsConfclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(JetCalibToolsConf.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

JetCalibToolsConfclean ::
#-- end of cleanup_header ---------------
