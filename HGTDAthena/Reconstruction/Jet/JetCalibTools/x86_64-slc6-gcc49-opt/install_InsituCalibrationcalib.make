#-- start of make_header -----------------

#====================================
#  Document install_InsituCalibrationcalib
#
#   Generated Wed Jun 21 12:06:31 2017  by woodsn
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_install_InsituCalibrationcalib_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_install_InsituCalibrationcalib_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_install_InsituCalibrationcalib

JetCalibTools_tag = $(tag)

#cmt_local_tagfile_install_InsituCalibrationcalib = $(JetCalibTools_tag)_install_InsituCalibrationcalib.make
cmt_local_tagfile_install_InsituCalibrationcalib = $(bin)$(JetCalibTools_tag)_install_InsituCalibrationcalib.make

else

tags      = $(tag),$(CMTEXTRATAGS)

JetCalibTools_tag = $(tag)

#cmt_local_tagfile_install_InsituCalibrationcalib = $(JetCalibTools_tag).make
cmt_local_tagfile_install_InsituCalibrationcalib = $(bin)$(JetCalibTools_tag).make

endif

include $(cmt_local_tagfile_install_InsituCalibrationcalib)
#-include $(cmt_local_tagfile_install_InsituCalibrationcalib)

ifdef cmt_install_InsituCalibrationcalib_has_target_tag

cmt_final_setup_install_InsituCalibrationcalib = $(bin)setup_install_InsituCalibrationcalib.make
cmt_dependencies_in_install_InsituCalibrationcalib = $(bin)dependencies_install_InsituCalibrationcalib.in
#cmt_final_setup_install_InsituCalibrationcalib = $(bin)JetCalibTools_install_InsituCalibrationcalibsetup.make
cmt_local_install_InsituCalibrationcalib_makefile = $(bin)install_InsituCalibrationcalib.make

else

cmt_final_setup_install_InsituCalibrationcalib = $(bin)setup.make
cmt_dependencies_in_install_InsituCalibrationcalib = $(bin)dependencies.in
#cmt_final_setup_install_InsituCalibrationcalib = $(bin)JetCalibToolssetup.make
cmt_local_install_InsituCalibrationcalib_makefile = $(bin)install_InsituCalibrationcalib.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)JetCalibToolssetup.make

#install_InsituCalibrationcalib :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'install_InsituCalibrationcalib'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = install_InsituCalibrationcalib/
#install_InsituCalibrationcalib::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------


ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/share/JetCalibTools/InsituCalibration

install_InsituCalibrationcalib :: install_InsituCalibrationcalibinstall ;

install :: install_InsituCalibrationcalibinstall ;

install_InsituCalibrationcalibclean :: install_InsituCalibrationcalibuninstall

uninstall :: install_InsituCalibrationcalibuninstall


# This is to avoid error in case there are no files to install
# Ideally, the fragment should not be used without files to install,
# and this line should be dropped then
install_InsituCalibrationcalibinstall :: ;

#-- start of cleanup_header --------------

clean :: install_InsituCalibrationcalibclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(install_InsituCalibrationcalib.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

install_InsituCalibrationcalibclean ::
#-- end of cleanup_header ---------------
