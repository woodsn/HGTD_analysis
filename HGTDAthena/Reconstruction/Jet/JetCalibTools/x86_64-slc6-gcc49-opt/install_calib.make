#-- start of make_header -----------------

#====================================
#  Document install_calib
#
#   Generated Wed Jun 21 12:06:31 2017  by woodsn
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_install_calib_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_install_calib_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_install_calib

JetCalibTools_tag = $(tag)

#cmt_local_tagfile_install_calib = $(JetCalibTools_tag)_install_calib.make
cmt_local_tagfile_install_calib = $(bin)$(JetCalibTools_tag)_install_calib.make

else

tags      = $(tag),$(CMTEXTRATAGS)

JetCalibTools_tag = $(tag)

#cmt_local_tagfile_install_calib = $(JetCalibTools_tag).make
cmt_local_tagfile_install_calib = $(bin)$(JetCalibTools_tag).make

endif

include $(cmt_local_tagfile_install_calib)
#-include $(cmt_local_tagfile_install_calib)

ifdef cmt_install_calib_has_target_tag

cmt_final_setup_install_calib = $(bin)setup_install_calib.make
cmt_dependencies_in_install_calib = $(bin)dependencies_install_calib.in
#cmt_final_setup_install_calib = $(bin)JetCalibTools_install_calibsetup.make
cmt_local_install_calib_makefile = $(bin)install_calib.make

else

cmt_final_setup_install_calib = $(bin)setup.make
cmt_dependencies_in_install_calib = $(bin)dependencies.in
#cmt_final_setup_install_calib = $(bin)JetCalibToolssetup.make
cmt_local_install_calib_makefile = $(bin)install_calib.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)JetCalibToolssetup.make

#install_calib :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'install_calib'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = install_calib/
#install_calib::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------


ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/share/JetCalibTools

install_calib :: install_calibinstall ;

install :: install_calibinstall ;

install_calibclean :: install_calibuninstall

uninstall :: install_calibuninstall


# This is to avoid error in case there are no files to install
# Ideally, the fragment should not be used without files to install,
# and this line should be dropped then
install_calibinstall :: ;

#-- start of cleanup_header --------------

clean :: install_calibclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(install_calib.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

install_calibclean ::
#-- end of cleanup_header ---------------
