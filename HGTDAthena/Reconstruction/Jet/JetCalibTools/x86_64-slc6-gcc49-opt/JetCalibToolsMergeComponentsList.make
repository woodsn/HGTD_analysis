#-- start of make_header -----------------

#====================================
#  Document JetCalibToolsMergeComponentsList
#
#   Generated Wed Jun 21 12:07:10 2017  by woodsn
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_JetCalibToolsMergeComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_JetCalibToolsMergeComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_JetCalibToolsMergeComponentsList

JetCalibTools_tag = $(tag)

#cmt_local_tagfile_JetCalibToolsMergeComponentsList = $(JetCalibTools_tag)_JetCalibToolsMergeComponentsList.make
cmt_local_tagfile_JetCalibToolsMergeComponentsList = $(bin)$(JetCalibTools_tag)_JetCalibToolsMergeComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

JetCalibTools_tag = $(tag)

#cmt_local_tagfile_JetCalibToolsMergeComponentsList = $(JetCalibTools_tag).make
cmt_local_tagfile_JetCalibToolsMergeComponentsList = $(bin)$(JetCalibTools_tag).make

endif

include $(cmt_local_tagfile_JetCalibToolsMergeComponentsList)
#-include $(cmt_local_tagfile_JetCalibToolsMergeComponentsList)

ifdef cmt_JetCalibToolsMergeComponentsList_has_target_tag

cmt_final_setup_JetCalibToolsMergeComponentsList = $(bin)setup_JetCalibToolsMergeComponentsList.make
cmt_dependencies_in_JetCalibToolsMergeComponentsList = $(bin)dependencies_JetCalibToolsMergeComponentsList.in
#cmt_final_setup_JetCalibToolsMergeComponentsList = $(bin)JetCalibTools_JetCalibToolsMergeComponentsListsetup.make
cmt_local_JetCalibToolsMergeComponentsList_makefile = $(bin)JetCalibToolsMergeComponentsList.make

else

cmt_final_setup_JetCalibToolsMergeComponentsList = $(bin)setup.make
cmt_dependencies_in_JetCalibToolsMergeComponentsList = $(bin)dependencies.in
#cmt_final_setup_JetCalibToolsMergeComponentsList = $(bin)JetCalibToolssetup.make
cmt_local_JetCalibToolsMergeComponentsList_makefile = $(bin)JetCalibToolsMergeComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)JetCalibToolssetup.make

#JetCalibToolsMergeComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'JetCalibToolsMergeComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = JetCalibToolsMergeComponentsList/
#JetCalibToolsMergeComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/merge_componentslist_header
# Author: Sebastien Binet (binet@cern.ch)

# Makefile fragment to merge a <library>.components file into a single
# <project>.components file in the (lib) install area
# If no InstallArea is present the fragment is dummy


.PHONY: JetCalibToolsMergeComponentsList JetCalibToolsMergeComponentsListclean

# default is already '#'
#genmap_comment_char := "'#'"

componentsListRef    := ../$(tag)/JetCalibTools.components

ifdef CMTINSTALLAREA
componentsListDir    := ${CMTINSTALLAREA}/$(tag)/lib
mergedComponentsList := $(componentsListDir)/$(project).components
stampComponentsList  := $(componentsListRef).stamp
else
componentsListDir    := ../$(tag)
mergedComponentsList :=
stampComponentsList  :=
endif

JetCalibToolsMergeComponentsList :: $(stampComponentsList) $(mergedComponentsList)
	@:

.NOTPARALLEL : $(stampComponentsList) $(mergedComponentsList)

$(stampComponentsList) $(mergedComponentsList) :: $(componentsListRef)
	@echo "Running merge_componentslist  JetCalibToolsMergeComponentsList"
	$(merge_componentslist_cmd) --do-merge \
         --input-file $(componentsListRef) --merged-file $(mergedComponentsList)

JetCalibToolsMergeComponentsListclean ::
	$(cleanup_silent) $(merge_componentslist_cmd) --un-merge \
         --input-file $(componentsListRef) --merged-file $(mergedComponentsList) ;
	$(cleanup_silent) $(remove_command) $(stampComponentsList)
libJetCalibTools_so_dependencies = ../x86_64-slc6-gcc49-opt/libJetCalibTools.so
#-- start of cleanup_header --------------

clean :: JetCalibToolsMergeComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(JetCalibToolsMergeComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

JetCalibToolsMergeComponentsListclean ::
#-- end of cleanup_header ---------------
