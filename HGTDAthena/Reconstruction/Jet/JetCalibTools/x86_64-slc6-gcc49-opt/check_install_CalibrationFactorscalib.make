#-- start of make_header -----------------

#====================================
#  Document check_install_CalibrationFactorscalib
#
#   Generated Wed Jun 21 12:06:31 2017  by woodsn
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_check_install_CalibrationFactorscalib_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_check_install_CalibrationFactorscalib_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_check_install_CalibrationFactorscalib

JetCalibTools_tag = $(tag)

#cmt_local_tagfile_check_install_CalibrationFactorscalib = $(JetCalibTools_tag)_check_install_CalibrationFactorscalib.make
cmt_local_tagfile_check_install_CalibrationFactorscalib = $(bin)$(JetCalibTools_tag)_check_install_CalibrationFactorscalib.make

else

tags      = $(tag),$(CMTEXTRATAGS)

JetCalibTools_tag = $(tag)

#cmt_local_tagfile_check_install_CalibrationFactorscalib = $(JetCalibTools_tag).make
cmt_local_tagfile_check_install_CalibrationFactorscalib = $(bin)$(JetCalibTools_tag).make

endif

include $(cmt_local_tagfile_check_install_CalibrationFactorscalib)
#-include $(cmt_local_tagfile_check_install_CalibrationFactorscalib)

ifdef cmt_check_install_CalibrationFactorscalib_has_target_tag

cmt_final_setup_check_install_CalibrationFactorscalib = $(bin)setup_check_install_CalibrationFactorscalib.make
cmt_dependencies_in_check_install_CalibrationFactorscalib = $(bin)dependencies_check_install_CalibrationFactorscalib.in
#cmt_final_setup_check_install_CalibrationFactorscalib = $(bin)JetCalibTools_check_install_CalibrationFactorscalibsetup.make
cmt_local_check_install_CalibrationFactorscalib_makefile = $(bin)check_install_CalibrationFactorscalib.make

else

cmt_final_setup_check_install_CalibrationFactorscalib = $(bin)setup.make
cmt_dependencies_in_check_install_CalibrationFactorscalib = $(bin)dependencies.in
#cmt_final_setup_check_install_CalibrationFactorscalib = $(bin)JetCalibToolssetup.make
cmt_local_check_install_CalibrationFactorscalib_makefile = $(bin)check_install_CalibrationFactorscalib.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)JetCalibToolssetup.make

#check_install_CalibrationFactorscalib :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'check_install_CalibrationFactorscalib'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = check_install_CalibrationFactorscalib/
#check_install_CalibrationFactorscalib::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of cmt_action_runner_header ---------------

ifdef ONCE
check_install_CalibrationFactorscalib_once = 1
endif

ifdef check_install_CalibrationFactorscalib_once

check_install_CalibrationFactorscalibactionstamp = $(bin)check_install_CalibrationFactorscalib.actionstamp
#check_install_CalibrationFactorscalibactionstamp = check_install_CalibrationFactorscalib.actionstamp

check_install_CalibrationFactorscalib :: $(check_install_CalibrationFactorscalibactionstamp)
	$(echo) "check_install_CalibrationFactorscalib ok"
#	@echo check_install_CalibrationFactorscalib ok

#$(check_install_CalibrationFactorscalibactionstamp) :: $(check_install_CalibrationFactorscalib_dependencies)
$(check_install_CalibrationFactorscalibactionstamp) ::
	$(silent) /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.7/AtlasCore/20.20.7/External/ExternalPolicy/cmt/atlas_check_installations.sh -files=' ../data/CalibrationFactors/*.config ../data/CalibrationFactors/*.root ' -installdir=/export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/InstallArea/share/JetCalibTools/CalibrationFactors -level=
	$(silent) cat /dev/null > $(check_install_CalibrationFactorscalibactionstamp)
#	@echo ok > $(check_install_CalibrationFactorscalibactionstamp)

check_install_CalibrationFactorscalibclean ::
	$(cleanup_silent) /bin/rm -f $(check_install_CalibrationFactorscalibactionstamp)

else

#check_install_CalibrationFactorscalib :: $(check_install_CalibrationFactorscalib_dependencies)
check_install_CalibrationFactorscalib ::
	$(silent) /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.7/AtlasCore/20.20.7/External/ExternalPolicy/cmt/atlas_check_installations.sh -files=' ../data/CalibrationFactors/*.config ../data/CalibrationFactors/*.root ' -installdir=/export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/InstallArea/share/JetCalibTools/CalibrationFactors -level=

endif

install ::
uninstall ::

#-- end of cmt_action_runner_header -----------------
#-- start of cleanup_header --------------

clean :: check_install_CalibrationFactorscalibclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(check_install_CalibrationFactorscalib.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

check_install_CalibrationFactorscalibclean ::
#-- end of cleanup_header ---------------
