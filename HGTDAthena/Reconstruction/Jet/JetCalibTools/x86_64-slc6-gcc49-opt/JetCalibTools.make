#-- start of make_header -----------------

#====================================
#  Library JetCalibTools
#
#   Generated Wed Jun 21 12:06:31 2017  by woodsn
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_JetCalibTools_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_JetCalibTools_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_JetCalibTools

JetCalibTools_tag = $(tag)

#cmt_local_tagfile_JetCalibTools = $(JetCalibTools_tag)_JetCalibTools.make
cmt_local_tagfile_JetCalibTools = $(bin)$(JetCalibTools_tag)_JetCalibTools.make

else

tags      = $(tag),$(CMTEXTRATAGS)

JetCalibTools_tag = $(tag)

#cmt_local_tagfile_JetCalibTools = $(JetCalibTools_tag).make
cmt_local_tagfile_JetCalibTools = $(bin)$(JetCalibTools_tag).make

endif

include $(cmt_local_tagfile_JetCalibTools)
#-include $(cmt_local_tagfile_JetCalibTools)

ifdef cmt_JetCalibTools_has_target_tag

cmt_final_setup_JetCalibTools = $(bin)setup_JetCalibTools.make
cmt_dependencies_in_JetCalibTools = $(bin)dependencies_JetCalibTools.in
#cmt_final_setup_JetCalibTools = $(bin)JetCalibTools_JetCalibToolssetup.make
cmt_local_JetCalibTools_makefile = $(bin)JetCalibTools.make

else

cmt_final_setup_JetCalibTools = $(bin)setup.make
cmt_dependencies_in_JetCalibTools = $(bin)dependencies.in
#cmt_final_setup_JetCalibTools = $(bin)JetCalibToolssetup.make
cmt_local_JetCalibTools_makefile = $(bin)JetCalibTools.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)JetCalibToolssetup.make

#JetCalibTools :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'JetCalibTools'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = JetCalibTools/
#JetCalibTools::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

JetCalibToolslibname   = $(bin)$(library_prefix)JetCalibTools$(library_suffix)
JetCalibToolslib       = $(JetCalibToolslibname).a
JetCalibToolsstamp     = $(bin)JetCalibTools.stamp
JetCalibToolsshstamp   = $(bin)JetCalibTools.shstamp

JetCalibTools :: dirs  JetCalibToolsLIB
	$(echo) "JetCalibTools ok"

#-- end of libary_header ----------------
#-- start of library_no_static ------

#JetCalibToolsLIB :: $(JetCalibToolslib) $(JetCalibToolsshstamp)
JetCalibToolsLIB :: $(JetCalibToolsshstamp)
	$(echo) "JetCalibTools : library ok"

$(JetCalibToolslib) :: $(bin)JetCalibUtils.o $(bin)JetCalibrationToolBase.o $(bin)JetPileupCorrection.o $(bin)NPVBeamspotCorrection.o $(bin)EtaJESCorrection.o $(bin)JMSCorrection.o $(bin)InsituDataCorrection.o $(bin)JetCalibrationTool.o $(bin)ResidualOffsetCorrection.o $(bin)GlobalSequentialCorrection.o $(bin)JetCalibTools_entries.o $(bin)JetCalibTools_load.o
	$(lib_echo) "static library $@"
	$(lib_silent) cd $(bin); \
	  $(ar) $(JetCalibToolslib) $?
	$(lib_silent) $(ranlib) $(JetCalibToolslib)
	$(lib_silent) cat /dev/null >$(JetCalibToolsstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

#
# We add one level of dependency upon the true shared library 
# (rather than simply upon the stamp file)
# this is for cases where the shared library has not been built
# while the stamp was created (error??) 
#

$(JetCalibToolslibname).$(shlibsuffix) :: $(bin)JetCalibUtils.o $(bin)JetCalibrationToolBase.o $(bin)JetPileupCorrection.o $(bin)NPVBeamspotCorrection.o $(bin)EtaJESCorrection.o $(bin)JMSCorrection.o $(bin)InsituDataCorrection.o $(bin)JetCalibrationTool.o $(bin)ResidualOffsetCorrection.o $(bin)GlobalSequentialCorrection.o $(bin)JetCalibTools_entries.o $(bin)JetCalibTools_load.o $(use_requirements) $(JetCalibToolsstamps)
	$(lib_echo) "shared library $@"
	$(lib_silent) $(shlibbuilder) $(shlibflags) -o $@ $(bin)JetCalibUtils.o $(bin)JetCalibrationToolBase.o $(bin)JetPileupCorrection.o $(bin)NPVBeamspotCorrection.o $(bin)EtaJESCorrection.o $(bin)JMSCorrection.o $(bin)InsituDataCorrection.o $(bin)JetCalibrationTool.o $(bin)ResidualOffsetCorrection.o $(bin)GlobalSequentialCorrection.o $(bin)JetCalibTools_entries.o $(bin)JetCalibTools_load.o $(JetCalibTools_shlibflags)
	$(lib_silent) cat /dev/null >$(JetCalibToolsstamp) && \
	  cat /dev/null >$(JetCalibToolsshstamp)

$(JetCalibToolsshstamp) :: $(JetCalibToolslibname).$(shlibsuffix)
	$(lib_silent) if test -f $(JetCalibToolslibname).$(shlibsuffix) ; then \
	  cat /dev/null >$(JetCalibToolsstamp) && \
	  cat /dev/null >$(JetCalibToolsshstamp) ; fi

JetCalibToolsclean ::
	$(cleanup_echo) objects JetCalibTools
	$(cleanup_silent) /bin/rm -f $(bin)JetCalibUtils.o $(bin)JetCalibrationToolBase.o $(bin)JetPileupCorrection.o $(bin)NPVBeamspotCorrection.o $(bin)EtaJESCorrection.o $(bin)JMSCorrection.o $(bin)InsituDataCorrection.o $(bin)JetCalibrationTool.o $(bin)ResidualOffsetCorrection.o $(bin)GlobalSequentialCorrection.o $(bin)JetCalibTools_entries.o $(bin)JetCalibTools_load.o
	$(cleanup_silent) /bin/rm -f $(patsubst %.o,%.d,$(bin)JetCalibUtils.o $(bin)JetCalibrationToolBase.o $(bin)JetPileupCorrection.o $(bin)NPVBeamspotCorrection.o $(bin)EtaJESCorrection.o $(bin)JMSCorrection.o $(bin)InsituDataCorrection.o $(bin)JetCalibrationTool.o $(bin)ResidualOffsetCorrection.o $(bin)GlobalSequentialCorrection.o $(bin)JetCalibTools_entries.o $(bin)JetCalibTools_load.o) $(patsubst %.o,%.dep,$(bin)JetCalibUtils.o $(bin)JetCalibrationToolBase.o $(bin)JetPileupCorrection.o $(bin)NPVBeamspotCorrection.o $(bin)EtaJESCorrection.o $(bin)JMSCorrection.o $(bin)InsituDataCorrection.o $(bin)JetCalibrationTool.o $(bin)ResidualOffsetCorrection.o $(bin)GlobalSequentialCorrection.o $(bin)JetCalibTools_entries.o $(bin)JetCalibTools_load.o) $(patsubst %.o,%.d.stamp,$(bin)JetCalibUtils.o $(bin)JetCalibrationToolBase.o $(bin)JetPileupCorrection.o $(bin)NPVBeamspotCorrection.o $(bin)EtaJESCorrection.o $(bin)JMSCorrection.o $(bin)InsituDataCorrection.o $(bin)JetCalibrationTool.o $(bin)ResidualOffsetCorrection.o $(bin)GlobalSequentialCorrection.o $(bin)JetCalibTools_entries.o $(bin)JetCalibTools_load.o)
	$(cleanup_silent) cd $(bin); /bin/rm -rf JetCalibTools_deps JetCalibTools_dependencies.make

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

install_dir = ${CMTINSTALLAREA}/$(tag)/lib
JetCalibToolsinstallname = $(library_prefix)JetCalibTools$(library_suffix).$(shlibsuffix)

JetCalibTools :: JetCalibToolsinstall ;

install :: JetCalibToolsinstall ;

JetCalibToolsinstall :: $(install_dir)/$(JetCalibToolsinstallname)
ifdef CMTINSTALLAREA
	$(echo) "installation done"
endif

$(install_dir)/$(JetCalibToolsinstallname) :: $(bin)$(JetCalibToolsinstallname)
ifdef CMTINSTALLAREA
	$(install_silent) $(cmt_install_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(JetCalibToolsinstallname)" \
	    -out "$(install_dir)" \
	    -cmd "$(cmt_installarea_command)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

##JetCalibToolsclean :: JetCalibToolsuninstall

uninstall :: JetCalibToolsuninstall ;

JetCalibToolsuninstall ::
ifdef CMTINSTALLAREA
	$(cleanup_silent) $(cmt_uninstall_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(JetCalibToolsinstallname)" \
	    -out "$(install_dir)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

#-- end of library_no_static ------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),JetCalibToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)JetCalibUtils.d

$(bin)$(binobj)JetCalibUtils.d :

$(bin)$(binobj)JetCalibUtils.o : $(cmt_final_setup_JetCalibTools)

$(bin)$(binobj)JetCalibUtils.o : ../Root/JetCalibUtils.cxx
	$(cpp_echo) ../Root/JetCalibUtils.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(JetCalibUtils_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(JetCalibUtils_cppflags) $(JetCalibUtils_cxx_cppflags) -I../Root ../Root/JetCalibUtils.cxx
endif
endif

else
$(bin)JetCalibTools_dependencies.make : $(JetCalibUtils_cxx_dependencies)

$(bin)JetCalibTools_dependencies.make : ../Root/JetCalibUtils.cxx

$(bin)$(binobj)JetCalibUtils.o : $(JetCalibUtils_cxx_dependencies)
	$(cpp_echo) ../Root/JetCalibUtils.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(JetCalibUtils_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(JetCalibUtils_cppflags) $(JetCalibUtils_cxx_cppflags) -I../Root ../Root/JetCalibUtils.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),JetCalibToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)JetCalibrationToolBase.d

$(bin)$(binobj)JetCalibrationToolBase.d :

$(bin)$(binobj)JetCalibrationToolBase.o : $(cmt_final_setup_JetCalibTools)

$(bin)$(binobj)JetCalibrationToolBase.o : ../Root/JetCalibrationToolBase.cxx
	$(cpp_echo) ../Root/JetCalibrationToolBase.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(JetCalibrationToolBase_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(JetCalibrationToolBase_cppflags) $(JetCalibrationToolBase_cxx_cppflags) -I../Root ../Root/JetCalibrationToolBase.cxx
endif
endif

else
$(bin)JetCalibTools_dependencies.make : $(JetCalibrationToolBase_cxx_dependencies)

$(bin)JetCalibTools_dependencies.make : ../Root/JetCalibrationToolBase.cxx

$(bin)$(binobj)JetCalibrationToolBase.o : $(JetCalibrationToolBase_cxx_dependencies)
	$(cpp_echo) ../Root/JetCalibrationToolBase.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(JetCalibrationToolBase_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(JetCalibrationToolBase_cppflags) $(JetCalibrationToolBase_cxx_cppflags) -I../Root ../Root/JetCalibrationToolBase.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),JetCalibToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)JetPileupCorrection.d

$(bin)$(binobj)JetPileupCorrection.d :

$(bin)$(binobj)JetPileupCorrection.o : $(cmt_final_setup_JetCalibTools)

$(bin)$(binobj)JetPileupCorrection.o : ../Root/JetPileupCorrection.cxx
	$(cpp_echo) ../Root/JetPileupCorrection.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(JetPileupCorrection_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(JetPileupCorrection_cppflags) $(JetPileupCorrection_cxx_cppflags) -I../Root ../Root/JetPileupCorrection.cxx
endif
endif

else
$(bin)JetCalibTools_dependencies.make : $(JetPileupCorrection_cxx_dependencies)

$(bin)JetCalibTools_dependencies.make : ../Root/JetPileupCorrection.cxx

$(bin)$(binobj)JetPileupCorrection.o : $(JetPileupCorrection_cxx_dependencies)
	$(cpp_echo) ../Root/JetPileupCorrection.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(JetPileupCorrection_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(JetPileupCorrection_cppflags) $(JetPileupCorrection_cxx_cppflags) -I../Root ../Root/JetPileupCorrection.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),JetCalibToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)NPVBeamspotCorrection.d

$(bin)$(binobj)NPVBeamspotCorrection.d :

$(bin)$(binobj)NPVBeamspotCorrection.o : $(cmt_final_setup_JetCalibTools)

$(bin)$(binobj)NPVBeamspotCorrection.o : ../Root/NPVBeamspotCorrection.cxx
	$(cpp_echo) ../Root/NPVBeamspotCorrection.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(NPVBeamspotCorrection_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(NPVBeamspotCorrection_cppflags) $(NPVBeamspotCorrection_cxx_cppflags) -I../Root ../Root/NPVBeamspotCorrection.cxx
endif
endif

else
$(bin)JetCalibTools_dependencies.make : $(NPVBeamspotCorrection_cxx_dependencies)

$(bin)JetCalibTools_dependencies.make : ../Root/NPVBeamspotCorrection.cxx

$(bin)$(binobj)NPVBeamspotCorrection.o : $(NPVBeamspotCorrection_cxx_dependencies)
	$(cpp_echo) ../Root/NPVBeamspotCorrection.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(NPVBeamspotCorrection_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(NPVBeamspotCorrection_cppflags) $(NPVBeamspotCorrection_cxx_cppflags) -I../Root ../Root/NPVBeamspotCorrection.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),JetCalibToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)EtaJESCorrection.d

$(bin)$(binobj)EtaJESCorrection.d :

$(bin)$(binobj)EtaJESCorrection.o : $(cmt_final_setup_JetCalibTools)

$(bin)$(binobj)EtaJESCorrection.o : ../Root/EtaJESCorrection.cxx
	$(cpp_echo) ../Root/EtaJESCorrection.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(EtaJESCorrection_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(EtaJESCorrection_cppflags) $(EtaJESCorrection_cxx_cppflags) -I../Root ../Root/EtaJESCorrection.cxx
endif
endif

else
$(bin)JetCalibTools_dependencies.make : $(EtaJESCorrection_cxx_dependencies)

$(bin)JetCalibTools_dependencies.make : ../Root/EtaJESCorrection.cxx

$(bin)$(binobj)EtaJESCorrection.o : $(EtaJESCorrection_cxx_dependencies)
	$(cpp_echo) ../Root/EtaJESCorrection.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(EtaJESCorrection_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(EtaJESCorrection_cppflags) $(EtaJESCorrection_cxx_cppflags) -I../Root ../Root/EtaJESCorrection.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),JetCalibToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)JMSCorrection.d

$(bin)$(binobj)JMSCorrection.d :

$(bin)$(binobj)JMSCorrection.o : $(cmt_final_setup_JetCalibTools)

$(bin)$(binobj)JMSCorrection.o : ../Root/JMSCorrection.cxx
	$(cpp_echo) ../Root/JMSCorrection.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(JMSCorrection_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(JMSCorrection_cppflags) $(JMSCorrection_cxx_cppflags) -I../Root ../Root/JMSCorrection.cxx
endif
endif

else
$(bin)JetCalibTools_dependencies.make : $(JMSCorrection_cxx_dependencies)

$(bin)JetCalibTools_dependencies.make : ../Root/JMSCorrection.cxx

$(bin)$(binobj)JMSCorrection.o : $(JMSCorrection_cxx_dependencies)
	$(cpp_echo) ../Root/JMSCorrection.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(JMSCorrection_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(JMSCorrection_cppflags) $(JMSCorrection_cxx_cppflags) -I../Root ../Root/JMSCorrection.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),JetCalibToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)InsituDataCorrection.d

$(bin)$(binobj)InsituDataCorrection.d :

$(bin)$(binobj)InsituDataCorrection.o : $(cmt_final_setup_JetCalibTools)

$(bin)$(binobj)InsituDataCorrection.o : ../Root/InsituDataCorrection.cxx
	$(cpp_echo) ../Root/InsituDataCorrection.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(InsituDataCorrection_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(InsituDataCorrection_cppflags) $(InsituDataCorrection_cxx_cppflags) -I../Root ../Root/InsituDataCorrection.cxx
endif
endif

else
$(bin)JetCalibTools_dependencies.make : $(InsituDataCorrection_cxx_dependencies)

$(bin)JetCalibTools_dependencies.make : ../Root/InsituDataCorrection.cxx

$(bin)$(binobj)InsituDataCorrection.o : $(InsituDataCorrection_cxx_dependencies)
	$(cpp_echo) ../Root/InsituDataCorrection.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(InsituDataCorrection_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(InsituDataCorrection_cppflags) $(InsituDataCorrection_cxx_cppflags) -I../Root ../Root/InsituDataCorrection.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),JetCalibToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)JetCalibrationTool.d

$(bin)$(binobj)JetCalibrationTool.d :

$(bin)$(binobj)JetCalibrationTool.o : $(cmt_final_setup_JetCalibTools)

$(bin)$(binobj)JetCalibrationTool.o : ../Root/JetCalibrationTool.cxx
	$(cpp_echo) ../Root/JetCalibrationTool.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(JetCalibrationTool_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(JetCalibrationTool_cppflags) $(JetCalibrationTool_cxx_cppflags) -I../Root ../Root/JetCalibrationTool.cxx
endif
endif

else
$(bin)JetCalibTools_dependencies.make : $(JetCalibrationTool_cxx_dependencies)

$(bin)JetCalibTools_dependencies.make : ../Root/JetCalibrationTool.cxx

$(bin)$(binobj)JetCalibrationTool.o : $(JetCalibrationTool_cxx_dependencies)
	$(cpp_echo) ../Root/JetCalibrationTool.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(JetCalibrationTool_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(JetCalibrationTool_cppflags) $(JetCalibrationTool_cxx_cppflags) -I../Root ../Root/JetCalibrationTool.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),JetCalibToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ResidualOffsetCorrection.d

$(bin)$(binobj)ResidualOffsetCorrection.d :

$(bin)$(binobj)ResidualOffsetCorrection.o : $(cmt_final_setup_JetCalibTools)

$(bin)$(binobj)ResidualOffsetCorrection.o : ../Root/ResidualOffsetCorrection.cxx
	$(cpp_echo) ../Root/ResidualOffsetCorrection.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(ResidualOffsetCorrection_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(ResidualOffsetCorrection_cppflags) $(ResidualOffsetCorrection_cxx_cppflags) -I../Root ../Root/ResidualOffsetCorrection.cxx
endif
endif

else
$(bin)JetCalibTools_dependencies.make : $(ResidualOffsetCorrection_cxx_dependencies)

$(bin)JetCalibTools_dependencies.make : ../Root/ResidualOffsetCorrection.cxx

$(bin)$(binobj)ResidualOffsetCorrection.o : $(ResidualOffsetCorrection_cxx_dependencies)
	$(cpp_echo) ../Root/ResidualOffsetCorrection.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(ResidualOffsetCorrection_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(ResidualOffsetCorrection_cppflags) $(ResidualOffsetCorrection_cxx_cppflags) -I../Root ../Root/ResidualOffsetCorrection.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),JetCalibToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)GlobalSequentialCorrection.d

$(bin)$(binobj)GlobalSequentialCorrection.d :

$(bin)$(binobj)GlobalSequentialCorrection.o : $(cmt_final_setup_JetCalibTools)

$(bin)$(binobj)GlobalSequentialCorrection.o : ../Root/GlobalSequentialCorrection.cxx
	$(cpp_echo) ../Root/GlobalSequentialCorrection.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(GlobalSequentialCorrection_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(GlobalSequentialCorrection_cppflags) $(GlobalSequentialCorrection_cxx_cppflags) -I../Root ../Root/GlobalSequentialCorrection.cxx
endif
endif

else
$(bin)JetCalibTools_dependencies.make : $(GlobalSequentialCorrection_cxx_dependencies)

$(bin)JetCalibTools_dependencies.make : ../Root/GlobalSequentialCorrection.cxx

$(bin)$(binobj)GlobalSequentialCorrection.o : $(GlobalSequentialCorrection_cxx_dependencies)
	$(cpp_echo) ../Root/GlobalSequentialCorrection.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(GlobalSequentialCorrection_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(GlobalSequentialCorrection_cppflags) $(GlobalSequentialCorrection_cxx_cppflags) -I../Root ../Root/GlobalSequentialCorrection.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),JetCalibToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)JetCalibTools_entries.d

$(bin)$(binobj)JetCalibTools_entries.d :

$(bin)$(binobj)JetCalibTools_entries.o : $(cmt_final_setup_JetCalibTools)

$(bin)$(binobj)JetCalibTools_entries.o : $(src)components/JetCalibTools_entries.cxx
	$(cpp_echo) $(src)components/JetCalibTools_entries.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(JetCalibTools_entries_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(JetCalibTools_entries_cppflags) $(JetCalibTools_entries_cxx_cppflags) -I../src/components $(src)components/JetCalibTools_entries.cxx
endif
endif

else
$(bin)JetCalibTools_dependencies.make : $(JetCalibTools_entries_cxx_dependencies)

$(bin)JetCalibTools_dependencies.make : $(src)components/JetCalibTools_entries.cxx

$(bin)$(binobj)JetCalibTools_entries.o : $(JetCalibTools_entries_cxx_dependencies)
	$(cpp_echo) $(src)components/JetCalibTools_entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(JetCalibTools_entries_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(JetCalibTools_entries_cppflags) $(JetCalibTools_entries_cxx_cppflags) -I../src/components $(src)components/JetCalibTools_entries.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),JetCalibToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)JetCalibTools_load.d

$(bin)$(binobj)JetCalibTools_load.d :

$(bin)$(binobj)JetCalibTools_load.o : $(cmt_final_setup_JetCalibTools)

$(bin)$(binobj)JetCalibTools_load.o : $(src)components/JetCalibTools_load.cxx
	$(cpp_echo) $(src)components/JetCalibTools_load.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(JetCalibTools_load_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(JetCalibTools_load_cppflags) $(JetCalibTools_load_cxx_cppflags) -I../src/components $(src)components/JetCalibTools_load.cxx
endif
endif

else
$(bin)JetCalibTools_dependencies.make : $(JetCalibTools_load_cxx_dependencies)

$(bin)JetCalibTools_dependencies.make : $(src)components/JetCalibTools_load.cxx

$(bin)$(binobj)JetCalibTools_load.o : $(JetCalibTools_load_cxx_dependencies)
	$(cpp_echo) $(src)components/JetCalibTools_load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(JetCalibTools_pp_cppflags) $(lib_JetCalibTools_pp_cppflags) $(JetCalibTools_load_pp_cppflags) $(use_cppflags) $(JetCalibTools_cppflags) $(lib_JetCalibTools_cppflags) $(JetCalibTools_load_cppflags) $(JetCalibTools_load_cxx_cppflags) -I../src/components $(src)components/JetCalibTools_load.cxx

endif

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: JetCalibToolsclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(JetCalibTools.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

JetCalibToolsclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library JetCalibTools
	-$(cleanup_silent) cd $(bin) && \rm -f $(library_prefix)JetCalibTools$(library_suffix).a $(library_prefix)JetCalibTools$(library_suffix).$(shlibsuffix) JetCalibTools.stamp JetCalibTools.shstamp
#-- end of cleanup_library ---------------
