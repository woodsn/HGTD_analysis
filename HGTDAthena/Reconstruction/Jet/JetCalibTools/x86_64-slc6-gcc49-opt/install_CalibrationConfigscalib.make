#-- start of make_header -----------------

#====================================
#  Document install_CalibrationConfigscalib
#
#   Generated Wed Jun 21 12:06:31 2017  by woodsn
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_install_CalibrationConfigscalib_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_install_CalibrationConfigscalib_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_install_CalibrationConfigscalib

JetCalibTools_tag = $(tag)

#cmt_local_tagfile_install_CalibrationConfigscalib = $(JetCalibTools_tag)_install_CalibrationConfigscalib.make
cmt_local_tagfile_install_CalibrationConfigscalib = $(bin)$(JetCalibTools_tag)_install_CalibrationConfigscalib.make

else

tags      = $(tag),$(CMTEXTRATAGS)

JetCalibTools_tag = $(tag)

#cmt_local_tagfile_install_CalibrationConfigscalib = $(JetCalibTools_tag).make
cmt_local_tagfile_install_CalibrationConfigscalib = $(bin)$(JetCalibTools_tag).make

endif

include $(cmt_local_tagfile_install_CalibrationConfigscalib)
#-include $(cmt_local_tagfile_install_CalibrationConfigscalib)

ifdef cmt_install_CalibrationConfigscalib_has_target_tag

cmt_final_setup_install_CalibrationConfigscalib = $(bin)setup_install_CalibrationConfigscalib.make
cmt_dependencies_in_install_CalibrationConfigscalib = $(bin)dependencies_install_CalibrationConfigscalib.in
#cmt_final_setup_install_CalibrationConfigscalib = $(bin)JetCalibTools_install_CalibrationConfigscalibsetup.make
cmt_local_install_CalibrationConfigscalib_makefile = $(bin)install_CalibrationConfigscalib.make

else

cmt_final_setup_install_CalibrationConfigscalib = $(bin)setup.make
cmt_dependencies_in_install_CalibrationConfigscalib = $(bin)dependencies.in
#cmt_final_setup_install_CalibrationConfigscalib = $(bin)JetCalibToolssetup.make
cmt_local_install_CalibrationConfigscalib_makefile = $(bin)install_CalibrationConfigscalib.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)JetCalibToolssetup.make

#install_CalibrationConfigscalib :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'install_CalibrationConfigscalib'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = install_CalibrationConfigscalib/
#install_CalibrationConfigscalib::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------


ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/share/JetCalibTools/CalibrationConfigs

install_CalibrationConfigscalib :: install_CalibrationConfigscalibinstall ;

install :: install_CalibrationConfigscalibinstall ;

install_CalibrationConfigscalibclean :: install_CalibrationConfigscalibuninstall

uninstall :: install_CalibrationConfigscalibuninstall


# This is to avoid error in case there are no files to install
# Ideally, the fragment should not be used without files to install,
# and this line should be dropped then
install_CalibrationConfigscalibinstall :: ;

#-- start of cleanup_header --------------

clean :: install_CalibrationConfigscalibclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(install_CalibrationConfigscalib.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

install_CalibrationConfigscalibclean ::
#-- end of cleanup_header ---------------
