#-- start of make_header -----------------

#====================================
#  Document JetCalibToolsComponentsList
#
#   Generated Wed Jun 21 12:07:09 2017  by woodsn
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_JetCalibToolsComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_JetCalibToolsComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_JetCalibToolsComponentsList

JetCalibTools_tag = $(tag)

#cmt_local_tagfile_JetCalibToolsComponentsList = $(JetCalibTools_tag)_JetCalibToolsComponentsList.make
cmt_local_tagfile_JetCalibToolsComponentsList = $(bin)$(JetCalibTools_tag)_JetCalibToolsComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

JetCalibTools_tag = $(tag)

#cmt_local_tagfile_JetCalibToolsComponentsList = $(JetCalibTools_tag).make
cmt_local_tagfile_JetCalibToolsComponentsList = $(bin)$(JetCalibTools_tag).make

endif

include $(cmt_local_tagfile_JetCalibToolsComponentsList)
#-include $(cmt_local_tagfile_JetCalibToolsComponentsList)

ifdef cmt_JetCalibToolsComponentsList_has_target_tag

cmt_final_setup_JetCalibToolsComponentsList = $(bin)setup_JetCalibToolsComponentsList.make
cmt_dependencies_in_JetCalibToolsComponentsList = $(bin)dependencies_JetCalibToolsComponentsList.in
#cmt_final_setup_JetCalibToolsComponentsList = $(bin)JetCalibTools_JetCalibToolsComponentsListsetup.make
cmt_local_JetCalibToolsComponentsList_makefile = $(bin)JetCalibToolsComponentsList.make

else

cmt_final_setup_JetCalibToolsComponentsList = $(bin)setup.make
cmt_dependencies_in_JetCalibToolsComponentsList = $(bin)dependencies.in
#cmt_final_setup_JetCalibToolsComponentsList = $(bin)JetCalibToolssetup.make
cmt_local_JetCalibToolsComponentsList_makefile = $(bin)JetCalibToolsComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)JetCalibToolssetup.make

#JetCalibToolsComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'JetCalibToolsComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = JetCalibToolsComponentsList/
#JetCalibToolsComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
##
componentslistfile = JetCalibTools.components
COMPONENTSLIST_DIR = ../$(tag)
fulllibname = libJetCalibTools.$(shlibsuffix)

JetCalibToolsComponentsList :: ${COMPONENTSLIST_DIR}/$(componentslistfile)
	@:

${COMPONENTSLIST_DIR}/$(componentslistfile) :: $(bin)$(fulllibname)
	@echo 'Generating componentslist file for $(fulllibname)'
	cd ../$(tag);$(listcomponents_cmd) --output ${COMPONENTSLIST_DIR}/$(componentslistfile) $(fulllibname)

install :: JetCalibToolsComponentsListinstall
JetCalibToolsComponentsListinstall :: JetCalibToolsComponentsList

uninstall :: JetCalibToolsComponentsListuninstall
JetCalibToolsComponentsListuninstall :: JetCalibToolsComponentsListclean

JetCalibToolsComponentsListclean ::
	@echo 'Deleting $(componentslistfile)'
	@rm -f ${COMPONENTSLIST_DIR}/$(componentslistfile)

#-- start of cleanup_header --------------

clean :: JetCalibToolsComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(JetCalibToolsComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

JetCalibToolsComponentsListclean ::
#-- end of cleanup_header ---------------
