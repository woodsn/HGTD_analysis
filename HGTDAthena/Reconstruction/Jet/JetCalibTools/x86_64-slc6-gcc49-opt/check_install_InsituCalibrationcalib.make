#-- start of make_header -----------------

#====================================
#  Document check_install_InsituCalibrationcalib
#
#   Generated Wed Jun 21 12:06:31 2017  by woodsn
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_check_install_InsituCalibrationcalib_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_check_install_InsituCalibrationcalib_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_check_install_InsituCalibrationcalib

JetCalibTools_tag = $(tag)

#cmt_local_tagfile_check_install_InsituCalibrationcalib = $(JetCalibTools_tag)_check_install_InsituCalibrationcalib.make
cmt_local_tagfile_check_install_InsituCalibrationcalib = $(bin)$(JetCalibTools_tag)_check_install_InsituCalibrationcalib.make

else

tags      = $(tag),$(CMTEXTRATAGS)

JetCalibTools_tag = $(tag)

#cmt_local_tagfile_check_install_InsituCalibrationcalib = $(JetCalibTools_tag).make
cmt_local_tagfile_check_install_InsituCalibrationcalib = $(bin)$(JetCalibTools_tag).make

endif

include $(cmt_local_tagfile_check_install_InsituCalibrationcalib)
#-include $(cmt_local_tagfile_check_install_InsituCalibrationcalib)

ifdef cmt_check_install_InsituCalibrationcalib_has_target_tag

cmt_final_setup_check_install_InsituCalibrationcalib = $(bin)setup_check_install_InsituCalibrationcalib.make
cmt_dependencies_in_check_install_InsituCalibrationcalib = $(bin)dependencies_check_install_InsituCalibrationcalib.in
#cmt_final_setup_check_install_InsituCalibrationcalib = $(bin)JetCalibTools_check_install_InsituCalibrationcalibsetup.make
cmt_local_check_install_InsituCalibrationcalib_makefile = $(bin)check_install_InsituCalibrationcalib.make

else

cmt_final_setup_check_install_InsituCalibrationcalib = $(bin)setup.make
cmt_dependencies_in_check_install_InsituCalibrationcalib = $(bin)dependencies.in
#cmt_final_setup_check_install_InsituCalibrationcalib = $(bin)JetCalibToolssetup.make
cmt_local_check_install_InsituCalibrationcalib_makefile = $(bin)check_install_InsituCalibrationcalib.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)JetCalibToolssetup.make

#check_install_InsituCalibrationcalib :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'check_install_InsituCalibrationcalib'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = check_install_InsituCalibrationcalib/
#check_install_InsituCalibrationcalib::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of cmt_action_runner_header ---------------

ifdef ONCE
check_install_InsituCalibrationcalib_once = 1
endif

ifdef check_install_InsituCalibrationcalib_once

check_install_InsituCalibrationcalibactionstamp = $(bin)check_install_InsituCalibrationcalib.actionstamp
#check_install_InsituCalibrationcalibactionstamp = check_install_InsituCalibrationcalib.actionstamp

check_install_InsituCalibrationcalib :: $(check_install_InsituCalibrationcalibactionstamp)
	$(echo) "check_install_InsituCalibrationcalib ok"
#	@echo check_install_InsituCalibrationcalib ok

#$(check_install_InsituCalibrationcalibactionstamp) :: $(check_install_InsituCalibrationcalib_dependencies)
$(check_install_InsituCalibrationcalibactionstamp) ::
	$(silent) /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.7/AtlasCore/20.20.7/External/ExternalPolicy/cmt/atlas_check_installations.sh -files=' ../data/InsituCalibration/*.root ' -installdir=/export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/InstallArea/share/JetCalibTools/InsituCalibration -level=
	$(silent) cat /dev/null > $(check_install_InsituCalibrationcalibactionstamp)
#	@echo ok > $(check_install_InsituCalibrationcalibactionstamp)

check_install_InsituCalibrationcalibclean ::
	$(cleanup_silent) /bin/rm -f $(check_install_InsituCalibrationcalibactionstamp)

else

#check_install_InsituCalibrationcalib :: $(check_install_InsituCalibrationcalib_dependencies)
check_install_InsituCalibrationcalib ::
	$(silent) /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.7/AtlasCore/20.20.7/External/ExternalPolicy/cmt/atlas_check_installations.sh -files=' ../data/InsituCalibration/*.root ' -installdir=/export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/InstallArea/share/JetCalibTools/InsituCalibration -level=

endif

install ::
uninstall ::

#-- end of cmt_action_runner_header -----------------
#-- start of cleanup_header --------------

clean :: check_install_InsituCalibrationcalibclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(check_install_InsituCalibrationcalib.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

check_install_InsituCalibrationcalibclean ::
#-- end of cleanup_header ---------------
