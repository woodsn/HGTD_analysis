#-- start of make_header -----------------

#====================================
#  Document install_CalibrationFactorscalib
#
#   Generated Wed Jun 21 12:06:31 2017  by woodsn
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_install_CalibrationFactorscalib_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_install_CalibrationFactorscalib_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_install_CalibrationFactorscalib

JetCalibTools_tag = $(tag)

#cmt_local_tagfile_install_CalibrationFactorscalib = $(JetCalibTools_tag)_install_CalibrationFactorscalib.make
cmt_local_tagfile_install_CalibrationFactorscalib = $(bin)$(JetCalibTools_tag)_install_CalibrationFactorscalib.make

else

tags      = $(tag),$(CMTEXTRATAGS)

JetCalibTools_tag = $(tag)

#cmt_local_tagfile_install_CalibrationFactorscalib = $(JetCalibTools_tag).make
cmt_local_tagfile_install_CalibrationFactorscalib = $(bin)$(JetCalibTools_tag).make

endif

include $(cmt_local_tagfile_install_CalibrationFactorscalib)
#-include $(cmt_local_tagfile_install_CalibrationFactorscalib)

ifdef cmt_install_CalibrationFactorscalib_has_target_tag

cmt_final_setup_install_CalibrationFactorscalib = $(bin)setup_install_CalibrationFactorscalib.make
cmt_dependencies_in_install_CalibrationFactorscalib = $(bin)dependencies_install_CalibrationFactorscalib.in
#cmt_final_setup_install_CalibrationFactorscalib = $(bin)JetCalibTools_install_CalibrationFactorscalibsetup.make
cmt_local_install_CalibrationFactorscalib_makefile = $(bin)install_CalibrationFactorscalib.make

else

cmt_final_setup_install_CalibrationFactorscalib = $(bin)setup.make
cmt_dependencies_in_install_CalibrationFactorscalib = $(bin)dependencies.in
#cmt_final_setup_install_CalibrationFactorscalib = $(bin)JetCalibToolssetup.make
cmt_local_install_CalibrationFactorscalib_makefile = $(bin)install_CalibrationFactorscalib.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)JetCalibToolssetup.make

#install_CalibrationFactorscalib :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'install_CalibrationFactorscalib'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = install_CalibrationFactorscalib/
#install_CalibrationFactorscalib::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------


ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/share/JetCalibTools/CalibrationFactors

install_CalibrationFactorscalib :: install_CalibrationFactorscalibinstall ;

install :: install_CalibrationFactorscalibinstall ;

install_CalibrationFactorscalibclean :: install_CalibrationFactorscalibuninstall

uninstall :: install_CalibrationFactorscalibuninstall


# This is to avoid error in case there are no files to install
# Ideally, the fragment should not be used without files to install,
# and this line should be dropped then
install_CalibrationFactorscalibinstall :: ;

#-- start of cleanup_header --------------

clean :: install_CalibrationFactorscalibclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(install_CalibrationFactorscalib.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

install_CalibrationFactorscalibclean ::
#-- end of cleanup_header ---------------
