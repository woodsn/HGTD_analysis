import os
import re
from glob import glob
from AthenaCommon.AthenaCommonFlags  import athenaCommonFlags
from AthenaCommon.AppMgr import ServiceMgr
import AthenaPoolCnvSvc.ReadAthenaPool
include("AthAnalysisBaseComps/SuppressLogging.py")

from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

theApp.EvtMax = -1

TestAreaPath='.'
if os.environ.get('TestArea'):
    TestAreaPath = os.environ.get('TestArea')

athenaCommonFlags.FilesInput =  glob("/export/share/gauss/woodsn/Weitao_HGTD/HGTD_analysis/HGTDAthena/mc15_14TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.recon.AOD.e4956_s3072_s3059_r8956/*root.1")
ServiceMgr.EventSelector.InputCollections = athenaCommonFlags.FilesInput()

algseq = CfgMgr.AthSequencer("AthAlgSeq")                #gets the main AthSequencer
algseq += CfgMgr.MyAlg()

svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["MYNTUPLE DATAFILE='myoutput.root' OPT='RECREATE'"]

ToolSvc += CfgMgr.JetCalibrationTool("myJESTool",IsData=False,ConfigFile="HLLHC/JES_MC15_HLLHC_r7769_May2016_v2.config",CalibSequence="JetArea_Residual_Origin_EtaJES",JetCollection="AntiKt4EMTopo")

from AthenaCommon.GlobalFlags import globalflags

from RecExConfig.InputFilePeeker import inputFileSummary
globalflags.DataSource = 'data' if inputFileSummary['evt_type'][0] == "IS_DATA" else 'geant4'
globalflags.DetDescrVersion = inputFileSummary['geometry']

xmlTags = [["ATLAS-P2-ITK-09-01-01","InclBrl_4","GMX"],
           ["ATLAS-P2-ITK-09-02-01","InclBrl_4","GMX"],
           ["ATLAS-P2-ITK-10-01-01","InclBrl_4","GMX"],
           ["ATLAS-P2-ITK-10-02-01","InclBrl_4","GMX"],]

print "InputFile DetDescrVersion : " , globalflags.DetDescrVersion()


for geoTag, layoutDescr, gmx in xmlTags:
   if (globalflags.DetDescrVersion().startswith(geoTag)):
      print "preIncludes for ",layoutDescr, " layout"
      from InDetRecExample.InDetJobProperties import InDetFlags
      include('InDetSLHC_Example/preInclude.SLHC.SiliconOnly.Reco.py')
      include('InDetSLHC_Example/preInclude.SLHC_Setup_'+layoutDescr+'.py')
      if gmx=="GMX":
         print "preIncludes for GMX strip layout"
         include('InDetSLHC_Example/preInclude.SLHC_Setup_Strip_GMX.py')
         if geoTag=="ATLAS-P2-ITK-10-00-00" or geoTag=="ATLAS-P2-ITK-09-00-00" :
           include('InDetSLHC_Example/SLHC_Setup_Reco_TrackingGeometry.py')
         else:
           include('InDetSLHC_Example/SLHC_Setup_Reco_TrackingGeometry_GMX.py')
      else :
         include('InDetSLHC_Example/SLHC_Setup_Reco_TrackingGeometry.py')
      break

# Just turn on the detector components we need
from AthenaCommon.DetFlags import DetFlags
DetFlags.detdescr.all_setOff()
DetFlags.detdescr.SCT_setOn()
DetFlags.detdescr.BField_setOn()
DetFlags.detdescr.pixel_setOn()

# Set up geometry and BField
include("RecExCond/AllDet_detDescr.py")

from InDetSLHC_Example.SLHC_JobProperties import SLHC_Flags
SLHC_Flags.SLHC_Version = ''


from AthenaCommon.GlobalFlags import jobproperties
DetDescrVersion = jobproperties.Global.DetDescrVersion()

for geoTag, layoutDescr,gmx in xmlTags:
   if (globalflags.DetDescrVersion().startswith(geoTag)):
      print "postInclude for ",layoutDescr, " layout"
      include('InDetSLHC_Example/postInclude.SLHC_Setup_'+layoutDescr+'.py')
      break


