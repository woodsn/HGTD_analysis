theApp.EvtMax = -1
jps.AthenaCommonFlags.FilesInput  = [ "/eos/atlas/user/b/benitezj/HGTD/aod/LArHit_Jan2017InclBrlNominal_mu200/mc15_14TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.recon.AOD.e4956_s3072_s3059_r9073/AOD.10654113._000050.pool.root.1" ]
import AthenaRootComps.ReadAthenaxAODHybrid

algseq = CfgMgr.AthSequencer("AthAlgSeq")                #gets the main AthSequencer
algseq += CfgMgr.MyAlg()

ToolSvc += CfgMgr.JetCalibrationTool("myJESTool",IsData=False,ConfigFile="HLLHC/JES_MC15_HLLHC_r7769_May2016_v2.config",CalibSequence="JetArea_Residual_Origin_EtaJES",JetCollection="AntiKt4EMTopo")
