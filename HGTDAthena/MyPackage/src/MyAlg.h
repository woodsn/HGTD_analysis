#ifndef MYPACKAGE_MYALG_H
#define MYPACKAGE_MYALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "GaudiKernel/ToolHandle.h" //included under assumption you'll want to use some tools! Remove if you don't!
#include <vector>
#include <TH1.h>
#include <TTree.h>
#include <TFile.h>
#include "JetCalibTools/IJetCalibrationTool.h"
#include "GaudiKernel/Algorithm.h"

using namespace std;
class MyAlg: public ::AthAlgorithm { 
 public: 
  MyAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~MyAlg(); 

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();

  int eventCounter;

 private:
  ToolHandle<IJetCalibrationTool> m_jetCalibration;
 
  std::string outputName;
//  TFile *hfile;         //!
  TTree *myTree;        //!

  Int_t Vtx_ntrack;     //!
  Double_t Vtx_z;       //!

  Int_t RunNum;         //!
  Int_t EvtNum;         //!
  Int_t PrimVtx;        //!
  Double_t ActInteract; //!
  Double_t AvgInteract; //!
  Int_t bcID; 		//!
  Int_t JetNum;         //!
  Int_t TruthNum;       //!

  vector<double> *hit_x_vec;   //!
  vector<double> *hit_y_vec;   //!
  vector<double> *hit_s_vec;   //!
  vector<double> *hit_t_vec;   //!
  vector<double> *hit_e_vec;   //!

  vector<double> *jet_pt_vec;  //!
  vector<double> *jet_eta_vec; //!
  vector<double> *jet_phi_vec; //!
  vector<double> *jet_e_vec;   //!
  vector<double> *jet_m_vec;   //!
  vector<double> *truth_jet_pt_vec;  //!
  vector<double> *truth_jet_eta_vec; //!
  vector<double> *truth_jet_phi_vec; //!
  vector<double> *truth_jet_e_vec;   //!
  vector<double> *truth_jet_m_vec;   //!

  TH3D *hits_to_cellt; //!
  TH3D *hits_to_celle; //!

  void tree_clear(void); //!
  bool passDeadZone(float x,float y); //!
}; 

#endif //> !MYPACKAGE_MYALG_H
