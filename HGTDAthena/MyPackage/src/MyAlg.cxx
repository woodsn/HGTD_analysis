// MyPackage includes
#define ASU 198

#include "MyAlg.h"
#include "StoreGate/StoreGate.h"
#include "StoreGate/StoreGateSvc.h"
#include "GaudiKernel/ITHistSvc.h"
#include "LArSimEvent/LArHitContainer.h"
#include "CaloDetDescr/CaloDetDescrElement.h"
#include "CaloDetDescr/CaloDetDescrManager.h"
#include "CaloIdentifier/CaloIdManager.h"
#include "CaloIdentifier/HGTD_ID.h"
#include "JetCalibTools/IJetCalibrationTool.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODCore/ShallowAuxContainer.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODTracking/VertexContainer.h"

using namespace std;
MyAlg::MyAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAlgorithm( name, pSvcLocator ){

  //declareProperty( "Property", m_nProperty ); //example property declaration

}


MyAlg::~MyAlg() {}


StatusCode MyAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  //calibration tools
  m_jetCalibration.setTypeAndName("JetCalibrationTool/myJESTool");
  CHECK( m_jetCalibration.retrieve() ); //optional, just forces initializing the tool here instead of at first use
/*
  hfile = new TFile("mytuple.root","RECREATE");
  if(!hfile || hfile->IsZombie()){
    ATH_MSG_ERROR ("Failed to create the output file" );
    return StatusCode::FAILURE;
  }
  myTree = new TTree("myTree","myTree");
  myTree->SetDirectory(hfile);
*/
  ServiceHandle<ITHistSvc> histSvc("THistSvc",name()); 
  CHECK( histSvc.retrieve() );
  myTree = new TTree("myTree","myTree");
  CHECK( histSvc->regTree("/MYNTUPLE/myTree",myTree) );

  myTree->Branch("RunNum",&RunNum,"RunNum/I");
  myTree->Branch("EvtNum",&EvtNum,"EvtNum/I");
  myTree->Branch("JetNum",&JetNum,"JetNum/I");
  myTree->Branch("PrimVtx",&PrimVtx,"PrimVtx/I");
  myTree->Branch("bcID",&bcID,"bcID/I");
  myTree->Branch("ActInteract",&ActInteract,"ActInteract/D");
  myTree->Branch("AvgInteract",&AvgInteract,"AvgInteract/D");
  myTree->Branch("hit_x_vec","vector<double>",&hit_x_vec);
  myTree->Branch("hit_y_vec","vector<double>",&hit_y_vec);
  myTree->Branch("hit_s_vec","vector<double>",&hit_s_vec);
  myTree->Branch("hit_t_vec","vector<double>",&hit_t_vec);
  myTree->Branch("hit_e_vec","vector<double>",&hit_e_vec);
  myTree->Branch("jet_pt_vec","vector<double>",&jet_pt_vec);
  myTree->Branch("jet_eta_vec","vector<double>",&jet_eta_vec);
  myTree->Branch("jet_phi_vec","vector<double>",&jet_phi_vec);
  myTree->Branch("jet_e_vec","vector<double>",&jet_e_vec);
  myTree->Branch("jet_m_vec","vector<double>",&jet_m_vec);
  myTree->Branch("truth_jet_pt_vec","vector<double>",&truth_jet_pt_vec);
  myTree->Branch("truth_jet_eta_vec","vector<double>",&truth_jet_eta_vec);
  myTree->Branch("truth_jet_phi_vec","vector<double>",&truth_jet_phi_vec);
  myTree->Branch("truth_jet_e_vec","vector<double>",&truth_jet_e_vec);
  myTree->Branch("truth_jet_m_vec","vector<double>",&truth_jet_m_vec);

  myTree->Branch("Vtx_ntrack",&Vtx_ntrack,"Vtx_ntrack/I");
  myTree->Branch("Vtx_z",&Vtx_z,"Vtx_z/D");

  hit_x_vec    = new vector<double>();
  hit_y_vec    = new vector<double>();
  hit_s_vec    = new vector<double>();
  hit_t_vec    = new vector<double>();
  hit_e_vec    = new vector<double>();
  jet_pt_vec   = new vector<double>();
  jet_eta_vec  = new vector<double>();
  jet_phi_vec  = new vector<double>();
  jet_e_vec    = new vector<double>();
  jet_m_vec    = new vector<double>();
  truth_jet_pt_vec   = new vector<double>();
  truth_jet_eta_vec  = new vector<double>();
  truth_jet_phi_vec  = new vector<double>();
  truth_jet_e_vec    = new vector<double>();
  truth_jet_m_vec    = new vector<double>();

  return StatusCode::SUCCESS;
}

StatusCode MyAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

//  hfile->cd();
  myTree->Write();

//  hfile->Close();
//  delete hfile;

  return StatusCode::SUCCESS;
}

StatusCode MyAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");

  eventCounter++;
  //Clear or initialize the output branches each loop
  tree_clear();
  //----------------------------------------------------------------
  //                   Event information
  //----------------------------------------------------------------
  const xAOD::EventInfo* eventInfo = 0;
  ATH_CHECK(evtStore()->retrieve( eventInfo, "EventInfo"));
  // check if the event is data or MC
  bool isMC = false;
  if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
     isMC = true; // can do something with this later
  }

  if(isMC){
    int datasetID =  eventInfo->mcChannelNumber();
    double eventWeight = eventInfo->mcEventWeight() ;
  }

  //save to my tree --eventinfo
  RunNum = eventInfo->runNumber();
  EvtNum = eventInfo->eventNumber();
  ActInteract = eventInfo->actualInteractionsPerCrossing();
  AvgInteract = eventInfo->averageInteractionsPerCrossing();
  bcID = eventInfo->bcid();
  if(AvgInteract!=0 && bcID%80 < 12) return StatusCode::SUCCESS; //do not proceed
  //vertex information
  const xAOD::VertexContainer* vertex = 0;
  ATH_CHECK(evtStore()->retrieve( vertex, "PrimaryVertices"));
  for(unsigned int iv=0;iv<vertex->size();iv++){
     const xAOD::Vertex* vxcand = vertex->at(iv);
     if(vxcand->vertexType()==xAOD::VxType::PriVtx){
        PrimVtx = vxcand->nTrackParticles();
        Vtx_z = vxcand->z();
     }
  }

  // get jet container of interest
  int num_jet = 0;
  const xAOD::JetContainer* jets = 0;
  ATH_CHECK(evtStore()->retrieve( jets, "AntiKt4EMTopoJets" ));

  int num_truth = 0;
  const xAOD::JetContainer* truth_jets = 0;
  ATH_CHECK(evtStore()->retrieve( truth_jets, "AntiKt4TruthJets" ));

  // loop over the truth jets in the container
  xAOD::JetContainer::const_iterator truth_jet_itr = truth_jets->begin();
  xAOD::JetContainer::const_iterator truth_jet_end = truth_jets->end();

  // loop over the jets in the container
  xAOD::JetContainer::const_iterator jet_itr = jets->begin();
  xAOD::JetContainer::const_iterator jet_end = jets->end();

  for(num_jet = 0 ; jet_itr != jet_end; ++jet_itr ) {
    //temp particle information
    double jet_pt = -9999;
    double jet_eta=  999;
    double jet_phi= -999;
    double jet_e  = -9999;
    double jet_m  = -9999;
    //temp truth particle information
    double truth_jet_pt = -9999;
    double truth_jet_eta=  999;
    double truth_jet_phi= -999;
    double truth_jet_e  = -9999;
    double truth_jet_m  = -9999;

    xAOD::Jet *jet_Cor = 0;
    //MC correction
    m_jetCalibration->calibratedCopy(*(*jet_itr),jet_Cor); //make a calibrated copy
    if( (jet_Cor)->pt() * 0.001 >30 && fabs(jet_Cor->eta())>2.4 ){
	num_jet++;
        jet_pt = (jet_Cor)->pt() * 0.001; 
        jet_eta = (jet_Cor)->eta();
        jet_phi = (jet_Cor)->phi();
        jet_e = (jet_Cor)->e() * 0.001;
        jet_m = (jet_Cor)->m() * 0.001;
	//truth match
	for(num_truth = 0 ; truth_jet_itr != truth_jet_end && num_truth<1; ++truth_jet_itr ) {
	  truth_jet_pt = (*truth_jet_itr)->pt() * 0.001; 
	  truth_jet_eta = (*truth_jet_itr)->eta();
	  truth_jet_phi = (*truth_jet_itr)->phi();
	  truth_jet_e = (*truth_jet_itr)->e() * 0.001;
	  truth_jet_m = (*truth_jet_itr)->m() * 0.001;
	  double deltaR = sqrt( (jet_eta - truth_jet_eta)*(jet_eta - truth_jet_eta) + (jet_phi - truth_jet_phi)*(jet_phi - truth_jet_phi) );
	  if( (*truth_jet_itr)->pt() * 0.001 >10 && deltaR<0.3 ){
		num_truth++;
	  } // end truth match pt > 10 dR<0.3
	  else {  //no matched truth
	    truth_jet_pt = -9999; 
	    truth_jet_eta = 999;
	    truth_jet_phi = -999;
	    truth_jet_e = -9999;
	    truth_jet_m = -9999;
	  }  //end no mached
	}

        //pushback to vector
        jet_pt_vec->push_back(jet_pt);
        jet_eta_vec->push_back(jet_eta);
        jet_phi_vec->push_back(jet_phi);
        jet_e_vec->push_back(jet_e);
        jet_m_vec->push_back(jet_m);
	truth_jet_pt_vec->push_back(truth_jet_pt);
	truth_jet_eta_vec->push_back(truth_jet_eta);
	truth_jet_phi_vec->push_back(truth_jet_phi);
	truth_jet_e_vec->push_back(truth_jet_e);
	truth_jet_m_vec->push_back(truth_jet_m);
    } // end pt > 30
    delete jet_Cor;
  } // end for loop over jets
  JetNum = num_jet;

  ///get the hits
  const DataHandle<LArHitContainer> p_collection ;
  StatusCode sc =  StoreGate::instance().retrieve( p_collection,"HGTDDigitContainer_MC") ;
  if (sc.isFailure() || !p_collection) {
    ATH_MSG_DEBUG ("Failed to retrieve collection : "<<"HGTDDigitContainer_MC");
    return StatusCode::FAILURE;
  }

  ///Get the Identifier
  StoreGateSvc *detStore = StoreGate::pointer("DetectorStore");
  const DataHandle<CaloIdManager> caloIdMgr;
  if ((detStore->retrieve(caloIdMgr)).isFailure()) {
    ATH_MSG_ERROR(" Unable to retrieve CaloIdManager from DetectoreStore");
    return StatusCode::FAILURE;
  }
  const HGTD_ID *ID = caloIdMgr->getHGTD_ID();//CaloCell->hgtd_idHelper();


  ///fill the cells
  hits_to_cellt = new TH3D("hits_to_cellt","hits_to_cellt",1200,-600,600, 1200,-600,600, 8,-0.5,7.5);
  hits_to_celle = new TH3D("hits_to_celle","hits_to_celle",1200,-600,600, 1200,-600,600, 8,-0.5,7.5);
  for (LArHitContainer::const_iterator hi= p_collection->begin(); hi!=p_collection->end() ; hi++){
    //ATH_MSG_DEBUG ("LArHitRead::execute  cellID:" << (*hi)->cellID()  << "  energy:" << (*hi)->energy() << " time:" << (*hi)->time());

    int cellS=ID->barrel_ec((*hi)->cellID()) * ( ID->sampling((*hi)->cellID()) + 1);
    if( cellS==0 || abs(cellS) > 4 ){
      ATH_MSG_ERROR ("Failed to determine HGTD layer for hit");
      continue;
    }

    float hitE=(*hi)->energy();
    float hitt=(*hi)->time()+11.8;
    float hity=0.5*ID->y_index((*hi)->cellID());


    ///remove hits which are set to the overflow
    if(hitt>14.4) continue;

    ///hits are saved in local coordinates ( negative z hits have a rotation around Y-axis), so x -> -x
    float hitx=0.5*ID->x_index((*hi)->cellID())*(cellS<0?-1:1);

    //remove the dead zones
    if(!passDeadZone(hitx,hity)) continue;

    //convert hits to cells
    if(hitx == 600) hitx = hitx-0.0001;
    if(hity == 600) hity = hity-0.0001;
    int hit_nx = (int)( hitx + 600);
    int hit_ny = (int)( hity + 600);
    int hit_z = (int)( cellS -0.1 + 5 ) ;
    if(cellS>0) hit_z--;
    if(hit_z < 0 || hit_z > 7) cout<<"hit_z!!!!"<<hit_z<<endl;
    if(hit_nx< 0 || hit_nx> 1199) cout<<"hit_x!!!!"<<hit_nx<<"  "<<hitx<<endl;
    if(hit_ny< 0 || hit_ny> 1199) cout<<"hit_y!!!!"<<hit_ny<<"  "<<hity<<endl;

    double cell_e_hist = hits_to_celle->GetBinContent(hit_nx+1,hit_ny+1,hit_z+1);
    double cell_t_hist = hits_to_cellt->GetBinContent(hit_nx+1,hit_ny+1,hit_z+1);

    cell_t_hist = cell_t_hist * cell_e_hist + hitt * hitE;
    cell_e_hist = cell_e_hist + hitE;
    cell_t_hist = cell_t_hist / cell_e_hist;
    hits_to_celle->SetBinContent(hit_nx+1,hit_ny+1,hit_z+1,cell_e_hist);
    hits_to_cellt->SetBinContent(hit_nx+1,hit_ny+1,hit_z+1,cell_t_hist);
  }
  for(int il=0;il<8;il++){
      for(int ix=0;ix<1200;ix++){
        for(int iy=0;iy<1200;iy++){
          double cell_xtemp = ix-599.5;
          double cell_ytemp = iy-599.5;
          double cell_ttemp = hits_to_cellt->GetBinContent(ix+1,iy+1,il+1);
          double cell_etemp = hits_to_celle->GetBinContent(ix+1,iy+1,il+1);
          int cell_ztemp = 0;
	  if(il<3.5) cell_ztemp = il - 4;
	  if(il>3.5) cell_ztemp = il - 3;
          if(cell_ttemp > 0.01){
	    hit_x_vec->push_back(cell_xtemp);
	    hit_y_vec->push_back(cell_ytemp);
	    hit_s_vec->push_back(cell_ztemp);
	    hit_t_vec->push_back(cell_ttemp);
	    hit_e_vec->push_back(cell_etemp);
          }//pushback
        }//iy
      }//ix
  }// il
  delete hits_to_cellt;
  delete hits_to_celle;
  //*****************************Fill mytree**************************//
  if(JetNum > 0) myTree->Fill();
  return StatusCode::SUCCESS;
}

void MyAlg::tree_clear(void){
  //clean vector
  hit_x_vec->clear();
  hit_y_vec->clear();
  hit_s_vec->clear();
  hit_t_vec->clear();
  hit_e_vec->clear();
  jet_pt_vec->clear();
  jet_eta_vec->clear();
  jet_phi_vec->clear();
  jet_e_vec->clear();
  jet_m_vec->clear();
  truth_jet_pt_vec->clear();
  truth_jet_eta_vec->clear();
  truth_jet_phi_vec->clear();
  truth_jet_e_vec->clear();
  truth_jet_m_vec->clear();
  //initialize 
  RunNum = -1;
  EvtNum = -1;
  JetNum = -1;
  bcID   = -1;
  PrimVtx = -1;
  ActInteract = 0;
  AvgInteract = 0;

  Vtx_ntrack = -1;
  Vtx_z = -999;
}

/////////////////////////////// dead regions function

bool MyAlg::passDeadZone(float x,float y){
  //note: this removes G4 hits not HGTD cells
  //HGTD cells may currently be not aligned with these dead zones

  float ASU_boundary = 2.25;
  float LRDETSeparation = 1.5;
  float SENSOR_boundary = 0.75;

  ///Use ony absolute values since detector is symmetric in x->-x, and y->-y
  float X=fabs(x);
  float Y=fabs(y);

  ///remove inner square hole
  if(X<ASU/2 && Y<ASU/2) return 0;


  ////////////////////////////
  /// Remove horizontal dead lines
  /// based on first quadrant: https://indico.cern.ch/event/555345/ (Aboud page 7 and 11)
  //////////////////////////
  ///remove horizontal line above center ASU row
  if( 0.5*ASU - ASU_boundary < Y && Y < 0.5*ASU + ASU_boundary) return 0;

  ///remove horizontal line above third ASU row from top
  if( 1.5*ASU - ASU_boundary < Y && Y < 1.5*ASU + ASU_boundary) return 0;

  ///remove horizontal line above second ASU row from top
  if( 2.5*ASU - ASU_boundary < Y && Y < 2.5*ASU + ASU_boundary) return 0;

  ////////////////////////////
  /// Remove vertical dead lines
  /// based on first quadrant: https://indico.cern.ch/event/555345/ (Aboud page 7 and 11)
  //////////////////////////
  //remove Vertical division at x=0
  if(X < LRDETSeparation/2 + ASU_boundary) return 0; //1.5 mm is separation between L and R detector halves

  //remove first ASU boundary (only above y=100)
  if(0.5*ASU < Y &&
     LRDETSeparation/2 + 1.0*ASU - ASU_boundary < X && X < LRDETSeparation/2 + 1.0*ASU + ASU_boundary) return 0;

  //remove second ASU boundary second ASU row from top
  if(1.5*ASU < Y && Y < 2.5*ASU &&
     LRDETSeparation/2 + 2.0*ASU - ASU_boundary < X && X < LRDETSeparation/2 + 2.0*ASU + ASU_boundary) return 0;

  //remove second ASU boundary for third and fourth ASU row from top
  if(Y < 1.5*ASU &&
     LRDETSeparation/2 + 1.5*ASU - ASU_boundary < X && X < LRDETSeparation/2 + 1.5*ASU + ASU_boundary) return 0;

  //remove last ASU boundary for third and fourth ASU rows from top
  if(Y < 1.5*ASU &&
     LRDETSeparation/2 + 2.5*ASU - ASU_boundary < X && X < LRDETSeparation/2 + 2.5*ASU + ASU_boundary) return 0;


  ////remove the dead zones between the sensors inside the ASU ( 4 sensors per squre ASU)
  int nasux=(X-LRDETSeparation/2)/(ASU/2);
  if( (X-LRDETSeparation/2-nasux*ASU/2) < SENSOR_boundary || (X-LRDETSeparation/2-nasux*ASU/2) > ASU/2-SENSOR_boundary ) return 0;

  int nasuy=Y/(ASU/2);
  if( (Y-nasuy*ASU/2) < SENSOR_boundary || (Y-nasuy*ASU/2) > ASU/2-SENSOR_boundary ) return 0;


  return 1;
}
