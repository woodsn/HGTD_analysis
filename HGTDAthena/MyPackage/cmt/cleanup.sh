# echo "cleanup MyPackage MyPackage-00-00-00 in /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.7/CMT/v1r25p20160527; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtMyPackagetempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtMyPackagetempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=MyPackage -version=MyPackage-00-00-00 -path=/export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena  $* >${cmtMyPackagetempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=MyPackage -version=MyPackage-00-00-00 -path=/export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena  $* >${cmtMyPackagetempfile}"
  cmtcleanupstatus=2
  /bin/rm -f ${cmtMyPackagetempfile}
  unset cmtMyPackagetempfile
  return $cmtcleanupstatus
fi
cmtcleanupstatus=0
. ${cmtMyPackagetempfile}
if test $? != 0 ; then
  cmtcleanupstatus=2
fi
/bin/rm -f ${cmtMyPackagetempfile}
unset cmtMyPackagetempfile
return $cmtcleanupstatus

