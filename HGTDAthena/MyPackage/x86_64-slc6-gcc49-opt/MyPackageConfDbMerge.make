#-- start of make_header -----------------

#====================================
#  Document MyPackageConfDbMerge
#
#   Generated Wed Jun 21 12:07:48 2017  by woodsn
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_MyPackageConfDbMerge_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_MyPackageConfDbMerge_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_MyPackageConfDbMerge

MyPackage_tag = $(tag)

#cmt_local_tagfile_MyPackageConfDbMerge = $(MyPackage_tag)_MyPackageConfDbMerge.make
cmt_local_tagfile_MyPackageConfDbMerge = $(bin)$(MyPackage_tag)_MyPackageConfDbMerge.make

else

tags      = $(tag),$(CMTEXTRATAGS)

MyPackage_tag = $(tag)

#cmt_local_tagfile_MyPackageConfDbMerge = $(MyPackage_tag).make
cmt_local_tagfile_MyPackageConfDbMerge = $(bin)$(MyPackage_tag).make

endif

include $(cmt_local_tagfile_MyPackageConfDbMerge)
#-include $(cmt_local_tagfile_MyPackageConfDbMerge)

ifdef cmt_MyPackageConfDbMerge_has_target_tag

cmt_final_setup_MyPackageConfDbMerge = $(bin)setup_MyPackageConfDbMerge.make
cmt_dependencies_in_MyPackageConfDbMerge = $(bin)dependencies_MyPackageConfDbMerge.in
#cmt_final_setup_MyPackageConfDbMerge = $(bin)MyPackage_MyPackageConfDbMergesetup.make
cmt_local_MyPackageConfDbMerge_makefile = $(bin)MyPackageConfDbMerge.make

else

cmt_final_setup_MyPackageConfDbMerge = $(bin)setup.make
cmt_dependencies_in_MyPackageConfDbMerge = $(bin)dependencies.in
#cmt_final_setup_MyPackageConfDbMerge = $(bin)MyPackagesetup.make
cmt_local_MyPackageConfDbMerge_makefile = $(bin)MyPackageConfDbMerge.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)MyPackagesetup.make

#MyPackageConfDbMerge :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'MyPackageConfDbMerge'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = MyPackageConfDbMerge/
#MyPackageConfDbMerge::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/merge_genconfDb_header
# Author: Sebastien Binet (binet@cern.ch)

# Makefile fragment to merge a <library>.confdb file into a single
# <project>.confdb file in the (lib) install area

.PHONY: MyPackageConfDbMerge MyPackageConfDbMergeclean

# default is already '#'
#genconfDb_comment_char := "'#'"

instdir      := ${CMTINSTALLAREA}/$(tag)
confDbRef    := /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/MyPackage/genConf/MyPackage/MyPackage.confdb
stampConfDb  := $(confDbRef).stamp
mergedConfDb := $(instdir)/lib/$(project).confdb

MyPackageConfDbMerge :: $(stampConfDb) $(mergedConfDb)
	@:

.NOTPARALLEL : $(stampConfDb) $(mergedConfDb)

$(stampConfDb) $(mergedConfDb) :: $(confDbRef)
	@echo "Running merge_genconfDb  MyPackageConfDbMerge"
	$(merge_genconfDb_cmd) \
          --do-merge \
          --input-file $(confDbRef) \
          --merged-file $(mergedConfDb)

MyPackageConfDbMergeclean ::
	$(cleanup_silent) $(merge_genconfDb_cmd) \
          --un-merge \
          --input-file $(confDbRef) \
          --merged-file $(mergedConfDb)	;
	$(cleanup_silent) $(remove_command) $(stampConfDb)
libMyPackage_so_dependencies = ../x86_64-slc6-gcc49-opt/libMyPackage.so
#-- start of cleanup_header --------------

clean :: MyPackageConfDbMergeclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(MyPackageConfDbMerge.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

MyPackageConfDbMergeclean ::
#-- end of cleanup_header ---------------
