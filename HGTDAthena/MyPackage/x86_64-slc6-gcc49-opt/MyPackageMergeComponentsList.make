#-- start of make_header -----------------

#====================================
#  Document MyPackageMergeComponentsList
#
#   Generated Wed Jun 21 12:07:49 2017  by woodsn
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_MyPackageMergeComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_MyPackageMergeComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_MyPackageMergeComponentsList

MyPackage_tag = $(tag)

#cmt_local_tagfile_MyPackageMergeComponentsList = $(MyPackage_tag)_MyPackageMergeComponentsList.make
cmt_local_tagfile_MyPackageMergeComponentsList = $(bin)$(MyPackage_tag)_MyPackageMergeComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

MyPackage_tag = $(tag)

#cmt_local_tagfile_MyPackageMergeComponentsList = $(MyPackage_tag).make
cmt_local_tagfile_MyPackageMergeComponentsList = $(bin)$(MyPackage_tag).make

endif

include $(cmt_local_tagfile_MyPackageMergeComponentsList)
#-include $(cmt_local_tagfile_MyPackageMergeComponentsList)

ifdef cmt_MyPackageMergeComponentsList_has_target_tag

cmt_final_setup_MyPackageMergeComponentsList = $(bin)setup_MyPackageMergeComponentsList.make
cmt_dependencies_in_MyPackageMergeComponentsList = $(bin)dependencies_MyPackageMergeComponentsList.in
#cmt_final_setup_MyPackageMergeComponentsList = $(bin)MyPackage_MyPackageMergeComponentsListsetup.make
cmt_local_MyPackageMergeComponentsList_makefile = $(bin)MyPackageMergeComponentsList.make

else

cmt_final_setup_MyPackageMergeComponentsList = $(bin)setup.make
cmt_dependencies_in_MyPackageMergeComponentsList = $(bin)dependencies.in
#cmt_final_setup_MyPackageMergeComponentsList = $(bin)MyPackagesetup.make
cmt_local_MyPackageMergeComponentsList_makefile = $(bin)MyPackageMergeComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)MyPackagesetup.make

#MyPackageMergeComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'MyPackageMergeComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = MyPackageMergeComponentsList/
#MyPackageMergeComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/merge_componentslist_header
# Author: Sebastien Binet (binet@cern.ch)

# Makefile fragment to merge a <library>.components file into a single
# <project>.components file in the (lib) install area
# If no InstallArea is present the fragment is dummy


.PHONY: MyPackageMergeComponentsList MyPackageMergeComponentsListclean

# default is already '#'
#genmap_comment_char := "'#'"

componentsListRef    := ../$(tag)/MyPackage.components

ifdef CMTINSTALLAREA
componentsListDir    := ${CMTINSTALLAREA}/$(tag)/lib
mergedComponentsList := $(componentsListDir)/$(project).components
stampComponentsList  := $(componentsListRef).stamp
else
componentsListDir    := ../$(tag)
mergedComponentsList :=
stampComponentsList  :=
endif

MyPackageMergeComponentsList :: $(stampComponentsList) $(mergedComponentsList)
	@:

.NOTPARALLEL : $(stampComponentsList) $(mergedComponentsList)

$(stampComponentsList) $(mergedComponentsList) :: $(componentsListRef)
	@echo "Running merge_componentslist  MyPackageMergeComponentsList"
	$(merge_componentslist_cmd) --do-merge \
         --input-file $(componentsListRef) --merged-file $(mergedComponentsList)

MyPackageMergeComponentsListclean ::
	$(cleanup_silent) $(merge_componentslist_cmd) --un-merge \
         --input-file $(componentsListRef) --merged-file $(mergedComponentsList) ;
	$(cleanup_silent) $(remove_command) $(stampComponentsList)
libMyPackage_so_dependencies = ../x86_64-slc6-gcc49-opt/libMyPackage.so
#-- start of cleanup_header --------------

clean :: MyPackageMergeComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(MyPackageMergeComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

MyPackageMergeComponentsListclean ::
#-- end of cleanup_header ---------------
