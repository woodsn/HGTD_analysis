#-- start of make_header -----------------

#====================================
#  Library MyPackage
#
#   Generated Wed Jun 21 12:07:15 2017  by woodsn
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_MyPackage_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_MyPackage_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_MyPackage

MyPackage_tag = $(tag)

#cmt_local_tagfile_MyPackage = $(MyPackage_tag)_MyPackage.make
cmt_local_tagfile_MyPackage = $(bin)$(MyPackage_tag)_MyPackage.make

else

tags      = $(tag),$(CMTEXTRATAGS)

MyPackage_tag = $(tag)

#cmt_local_tagfile_MyPackage = $(MyPackage_tag).make
cmt_local_tagfile_MyPackage = $(bin)$(MyPackage_tag).make

endif

include $(cmt_local_tagfile_MyPackage)
#-include $(cmt_local_tagfile_MyPackage)

ifdef cmt_MyPackage_has_target_tag

cmt_final_setup_MyPackage = $(bin)setup_MyPackage.make
cmt_dependencies_in_MyPackage = $(bin)dependencies_MyPackage.in
#cmt_final_setup_MyPackage = $(bin)MyPackage_MyPackagesetup.make
cmt_local_MyPackage_makefile = $(bin)MyPackage.make

else

cmt_final_setup_MyPackage = $(bin)setup.make
cmt_dependencies_in_MyPackage = $(bin)dependencies.in
#cmt_final_setup_MyPackage = $(bin)MyPackagesetup.make
cmt_local_MyPackage_makefile = $(bin)MyPackage.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)MyPackagesetup.make

#MyPackage :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'MyPackage'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = MyPackage/
#MyPackage::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

MyPackagelibname   = $(bin)$(library_prefix)MyPackage$(library_suffix)
MyPackagelib       = $(MyPackagelibname).a
MyPackagestamp     = $(bin)MyPackage.stamp
MyPackageshstamp   = $(bin)MyPackage.shstamp

MyPackage :: dirs  MyPackageLIB
	$(echo) "MyPackage ok"

#-- end of libary_header ----------------
#-- start of library_no_static ------

#MyPackageLIB :: $(MyPackagelib) $(MyPackageshstamp)
MyPackageLIB :: $(MyPackageshstamp)
	$(echo) "MyPackage : library ok"

$(MyPackagelib) :: $(bin)MyAlg.o $(bin)MyPackage_entries.o $(bin)MyPackage_load.o
	$(lib_echo) "static library $@"
	$(lib_silent) cd $(bin); \
	  $(ar) $(MyPackagelib) $?
	$(lib_silent) $(ranlib) $(MyPackagelib)
	$(lib_silent) cat /dev/null >$(MyPackagestamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

#
# We add one level of dependency upon the true shared library 
# (rather than simply upon the stamp file)
# this is for cases where the shared library has not been built
# while the stamp was created (error??) 
#

$(MyPackagelibname).$(shlibsuffix) :: $(bin)MyAlg.o $(bin)MyPackage_entries.o $(bin)MyPackage_load.o $(use_requirements) $(MyPackagestamps)
	$(lib_echo) "shared library $@"
	$(lib_silent) $(shlibbuilder) $(shlibflags) -o $@ $(bin)MyAlg.o $(bin)MyPackage_entries.o $(bin)MyPackage_load.o $(MyPackage_shlibflags)
	$(lib_silent) cat /dev/null >$(MyPackagestamp) && \
	  cat /dev/null >$(MyPackageshstamp)

$(MyPackageshstamp) :: $(MyPackagelibname).$(shlibsuffix)
	$(lib_silent) if test -f $(MyPackagelibname).$(shlibsuffix) ; then \
	  cat /dev/null >$(MyPackagestamp) && \
	  cat /dev/null >$(MyPackageshstamp) ; fi

MyPackageclean ::
	$(cleanup_echo) objects MyPackage
	$(cleanup_silent) /bin/rm -f $(bin)MyAlg.o $(bin)MyPackage_entries.o $(bin)MyPackage_load.o
	$(cleanup_silent) /bin/rm -f $(patsubst %.o,%.d,$(bin)MyAlg.o $(bin)MyPackage_entries.o $(bin)MyPackage_load.o) $(patsubst %.o,%.dep,$(bin)MyAlg.o $(bin)MyPackage_entries.o $(bin)MyPackage_load.o) $(patsubst %.o,%.d.stamp,$(bin)MyAlg.o $(bin)MyPackage_entries.o $(bin)MyPackage_load.o)
	$(cleanup_silent) cd $(bin); /bin/rm -rf MyPackage_deps MyPackage_dependencies.make

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

install_dir = ${CMTINSTALLAREA}/$(tag)/lib
MyPackageinstallname = $(library_prefix)MyPackage$(library_suffix).$(shlibsuffix)

MyPackage :: MyPackageinstall ;

install :: MyPackageinstall ;

MyPackageinstall :: $(install_dir)/$(MyPackageinstallname)
ifdef CMTINSTALLAREA
	$(echo) "installation done"
endif

$(install_dir)/$(MyPackageinstallname) :: $(bin)$(MyPackageinstallname)
ifdef CMTINSTALLAREA
	$(install_silent) $(cmt_install_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(MyPackageinstallname)" \
	    -out "$(install_dir)" \
	    -cmd "$(cmt_installarea_command)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

##MyPackageclean :: MyPackageuninstall

uninstall :: MyPackageuninstall ;

MyPackageuninstall ::
ifdef CMTINSTALLAREA
	$(cleanup_silent) $(cmt_uninstall_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(MyPackageinstallname)" \
	    -out "$(install_dir)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

#-- end of library_no_static ------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),MyPackageclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)MyAlg.d

$(bin)$(binobj)MyAlg.d :

$(bin)$(binobj)MyAlg.o : $(cmt_final_setup_MyPackage)

$(bin)$(binobj)MyAlg.o : $(src)MyAlg.cxx
	$(cpp_echo) $(src)MyAlg.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(MyPackage_pp_cppflags) $(lib_MyPackage_pp_cppflags) $(MyAlg_pp_cppflags) $(use_cppflags) $(MyPackage_cppflags) $(lib_MyPackage_cppflags) $(MyAlg_cppflags) $(MyAlg_cxx_cppflags)  $(src)MyAlg.cxx
endif
endif

else
$(bin)MyPackage_dependencies.make : $(MyAlg_cxx_dependencies)

$(bin)MyPackage_dependencies.make : $(src)MyAlg.cxx

$(bin)$(binobj)MyAlg.o : $(MyAlg_cxx_dependencies)
	$(cpp_echo) $(src)MyAlg.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(MyPackage_pp_cppflags) $(lib_MyPackage_pp_cppflags) $(MyAlg_pp_cppflags) $(use_cppflags) $(MyPackage_cppflags) $(lib_MyPackage_cppflags) $(MyAlg_cppflags) $(MyAlg_cxx_cppflags)  $(src)MyAlg.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),MyPackageclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)MyPackage_entries.d

$(bin)$(binobj)MyPackage_entries.d :

$(bin)$(binobj)MyPackage_entries.o : $(cmt_final_setup_MyPackage)

$(bin)$(binobj)MyPackage_entries.o : $(src)components/MyPackage_entries.cxx
	$(cpp_echo) $(src)components/MyPackage_entries.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(MyPackage_pp_cppflags) $(lib_MyPackage_pp_cppflags) $(MyPackage_entries_pp_cppflags) $(use_cppflags) $(MyPackage_cppflags) $(lib_MyPackage_cppflags) $(MyPackage_entries_cppflags) $(MyPackage_entries_cxx_cppflags) -I../src/components $(src)components/MyPackage_entries.cxx
endif
endif

else
$(bin)MyPackage_dependencies.make : $(MyPackage_entries_cxx_dependencies)

$(bin)MyPackage_dependencies.make : $(src)components/MyPackage_entries.cxx

$(bin)$(binobj)MyPackage_entries.o : $(MyPackage_entries_cxx_dependencies)
	$(cpp_echo) $(src)components/MyPackage_entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(MyPackage_pp_cppflags) $(lib_MyPackage_pp_cppflags) $(MyPackage_entries_pp_cppflags) $(use_cppflags) $(MyPackage_cppflags) $(lib_MyPackage_cppflags) $(MyPackage_entries_cppflags) $(MyPackage_entries_cxx_cppflags) -I../src/components $(src)components/MyPackage_entries.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),MyPackageclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)MyPackage_load.d

$(bin)$(binobj)MyPackage_load.d :

$(bin)$(binobj)MyPackage_load.o : $(cmt_final_setup_MyPackage)

$(bin)$(binobj)MyPackage_load.o : $(src)components/MyPackage_load.cxx
	$(cpp_echo) $(src)components/MyPackage_load.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(MyPackage_pp_cppflags) $(lib_MyPackage_pp_cppflags) $(MyPackage_load_pp_cppflags) $(use_cppflags) $(MyPackage_cppflags) $(lib_MyPackage_cppflags) $(MyPackage_load_cppflags) $(MyPackage_load_cxx_cppflags) -I../src/components $(src)components/MyPackage_load.cxx
endif
endif

else
$(bin)MyPackage_dependencies.make : $(MyPackage_load_cxx_dependencies)

$(bin)MyPackage_dependencies.make : $(src)components/MyPackage_load.cxx

$(bin)$(binobj)MyPackage_load.o : $(MyPackage_load_cxx_dependencies)
	$(cpp_echo) $(src)components/MyPackage_load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(MyPackage_pp_cppflags) $(lib_MyPackage_pp_cppflags) $(MyPackage_load_pp_cppflags) $(use_cppflags) $(MyPackage_cppflags) $(lib_MyPackage_cppflags) $(MyPackage_load_cppflags) $(MyPackage_load_cxx_cppflags) -I../src/components $(src)components/MyPackage_load.cxx

endif

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: MyPackageclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(MyPackage.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

MyPackageclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library MyPackage
	-$(cleanup_silent) cd $(bin) && \rm -f $(library_prefix)MyPackage$(library_suffix).a $(library_prefix)MyPackage$(library_suffix).$(shlibsuffix) MyPackage.stamp MyPackage.shstamp
#-- end of cleanup_library ---------------
