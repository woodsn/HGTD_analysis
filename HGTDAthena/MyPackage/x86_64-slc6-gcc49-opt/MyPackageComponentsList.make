#-- start of make_header -----------------

#====================================
#  Document MyPackageComponentsList
#
#   Generated Wed Jun 21 12:07:48 2017  by woodsn
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_MyPackageComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_MyPackageComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_MyPackageComponentsList

MyPackage_tag = $(tag)

#cmt_local_tagfile_MyPackageComponentsList = $(MyPackage_tag)_MyPackageComponentsList.make
cmt_local_tagfile_MyPackageComponentsList = $(bin)$(MyPackage_tag)_MyPackageComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

MyPackage_tag = $(tag)

#cmt_local_tagfile_MyPackageComponentsList = $(MyPackage_tag).make
cmt_local_tagfile_MyPackageComponentsList = $(bin)$(MyPackage_tag).make

endif

include $(cmt_local_tagfile_MyPackageComponentsList)
#-include $(cmt_local_tagfile_MyPackageComponentsList)

ifdef cmt_MyPackageComponentsList_has_target_tag

cmt_final_setup_MyPackageComponentsList = $(bin)setup_MyPackageComponentsList.make
cmt_dependencies_in_MyPackageComponentsList = $(bin)dependencies_MyPackageComponentsList.in
#cmt_final_setup_MyPackageComponentsList = $(bin)MyPackage_MyPackageComponentsListsetup.make
cmt_local_MyPackageComponentsList_makefile = $(bin)MyPackageComponentsList.make

else

cmt_final_setup_MyPackageComponentsList = $(bin)setup.make
cmt_dependencies_in_MyPackageComponentsList = $(bin)dependencies.in
#cmt_final_setup_MyPackageComponentsList = $(bin)MyPackagesetup.make
cmt_local_MyPackageComponentsList_makefile = $(bin)MyPackageComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)MyPackagesetup.make

#MyPackageComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'MyPackageComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = MyPackageComponentsList/
#MyPackageComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
##
componentslistfile = MyPackage.components
COMPONENTSLIST_DIR = ../$(tag)
fulllibname = libMyPackage.$(shlibsuffix)

MyPackageComponentsList :: ${COMPONENTSLIST_DIR}/$(componentslistfile)
	@:

${COMPONENTSLIST_DIR}/$(componentslistfile) :: $(bin)$(fulllibname)
	@echo 'Generating componentslist file for $(fulllibname)'
	cd ../$(tag);$(listcomponents_cmd) --output ${COMPONENTSLIST_DIR}/$(componentslistfile) $(fulllibname)

install :: MyPackageComponentsListinstall
MyPackageComponentsListinstall :: MyPackageComponentsList

uninstall :: MyPackageComponentsListuninstall
MyPackageComponentsListuninstall :: MyPackageComponentsListclean

MyPackageComponentsListclean ::
	@echo 'Deleting $(componentslistfile)'
	@rm -f ${COMPONENTSLIST_DIR}/$(componentslistfile)

#-- start of cleanup_header --------------

clean :: MyPackageComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(MyPackageComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

MyPackageComponentsListclean ::
#-- end of cleanup_header ---------------
