#-- start of make_header -----------------

#====================================
#  Document MyPackageCLIDDB
#
#   Generated Wed Jun 21 12:07:47 2017  by woodsn
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_MyPackageCLIDDB_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_MyPackageCLIDDB_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_MyPackageCLIDDB

MyPackage_tag = $(tag)

#cmt_local_tagfile_MyPackageCLIDDB = $(MyPackage_tag)_MyPackageCLIDDB.make
cmt_local_tagfile_MyPackageCLIDDB = $(bin)$(MyPackage_tag)_MyPackageCLIDDB.make

else

tags      = $(tag),$(CMTEXTRATAGS)

MyPackage_tag = $(tag)

#cmt_local_tagfile_MyPackageCLIDDB = $(MyPackage_tag).make
cmt_local_tagfile_MyPackageCLIDDB = $(bin)$(MyPackage_tag).make

endif

include $(cmt_local_tagfile_MyPackageCLIDDB)
#-include $(cmt_local_tagfile_MyPackageCLIDDB)

ifdef cmt_MyPackageCLIDDB_has_target_tag

cmt_final_setup_MyPackageCLIDDB = $(bin)setup_MyPackageCLIDDB.make
cmt_dependencies_in_MyPackageCLIDDB = $(bin)dependencies_MyPackageCLIDDB.in
#cmt_final_setup_MyPackageCLIDDB = $(bin)MyPackage_MyPackageCLIDDBsetup.make
cmt_local_MyPackageCLIDDB_makefile = $(bin)MyPackageCLIDDB.make

else

cmt_final_setup_MyPackageCLIDDB = $(bin)setup.make
cmt_dependencies_in_MyPackageCLIDDB = $(bin)dependencies.in
#cmt_final_setup_MyPackageCLIDDB = $(bin)MyPackagesetup.make
cmt_local_MyPackageCLIDDB_makefile = $(bin)MyPackageCLIDDB.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)MyPackagesetup.make

#MyPackageCLIDDB :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'MyPackageCLIDDB'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = MyPackageCLIDDB/
#MyPackageCLIDDB::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/genCLIDDB_header
# Author: Paolo Calafiura
# derived from genconf_header

# Use genCLIDDB_cmd to create package clid.db files

.PHONY: MyPackageCLIDDB MyPackageCLIDDBclean

outname := clid.db
cliddb  := MyPackage_$(outname)
instdir := $(CMTINSTALLAREA)/share
result  := $(instdir)/$(cliddb)
product := $(instdir)/$(outname)
conflib := $(bin)$(library_prefix)MyPackage.$(shlibsuffix)

MyPackageCLIDDB :: $(result)

$(instdir) :
	$(mkdir) -p $(instdir)

$(result) : $(conflib) $(product)
	@$(genCLIDDB_cmd) -p MyPackage -i$(product) -o $(result)

$(product) : $(instdir)
	touch $(product)

MyPackageCLIDDBclean ::
	$(cleanup_silent) $(uninstall_command) $(product) $(result)
	$(cleanup_silent) $(cmt_uninstallarea_command) $(product) $(result)

#-- start of cleanup_header --------------

clean :: MyPackageCLIDDBclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(MyPackageCLIDDB.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

MyPackageCLIDDBclean ::
#-- end of cleanup_header ---------------
