#-- start of make_header -----------------

#====================================
#  Document MyPackageConf
#
#   Generated Wed Jun 21 12:07:47 2017  by woodsn
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_MyPackageConf_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_MyPackageConf_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_MyPackageConf

MyPackage_tag = $(tag)

#cmt_local_tagfile_MyPackageConf = $(MyPackage_tag)_MyPackageConf.make
cmt_local_tagfile_MyPackageConf = $(bin)$(MyPackage_tag)_MyPackageConf.make

else

tags      = $(tag),$(CMTEXTRATAGS)

MyPackage_tag = $(tag)

#cmt_local_tagfile_MyPackageConf = $(MyPackage_tag).make
cmt_local_tagfile_MyPackageConf = $(bin)$(MyPackage_tag).make

endif

include $(cmt_local_tagfile_MyPackageConf)
#-include $(cmt_local_tagfile_MyPackageConf)

ifdef cmt_MyPackageConf_has_target_tag

cmt_final_setup_MyPackageConf = $(bin)setup_MyPackageConf.make
cmt_dependencies_in_MyPackageConf = $(bin)dependencies_MyPackageConf.in
#cmt_final_setup_MyPackageConf = $(bin)MyPackage_MyPackageConfsetup.make
cmt_local_MyPackageConf_makefile = $(bin)MyPackageConf.make

else

cmt_final_setup_MyPackageConf = $(bin)setup.make
cmt_dependencies_in_MyPackageConf = $(bin)dependencies.in
#cmt_final_setup_MyPackageConf = $(bin)MyPackagesetup.make
cmt_local_MyPackageConf_makefile = $(bin)MyPackageConf.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)MyPackagesetup.make

#MyPackageConf :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'MyPackageConf'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = MyPackageConf/
#MyPackageConf::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/genconfig_header
# Author: Wim Lavrijsen (WLavrijsen@lbl.gov)

# Use genconf.exe to create configurables python modules, then have the
# normal python install procedure take over.

.PHONY: MyPackageConf MyPackageConfclean

confpy  := MyPackageConf.py
conflib := $(bin)$(library_prefix)MyPackage.$(shlibsuffix)
confdb  := MyPackage.confdb
instdir := $(CMTINSTALLAREA)$(shared_install_subdir)/python/$(package)
product := $(instdir)/$(confpy)
initpy  := $(instdir)/__init__.py

ifdef GENCONF_ECHO
genconf_silent =
else
genconf_silent = $(silent)
endif

MyPackageConf :: MyPackageConfinstall

install :: MyPackageConfinstall

MyPackageConfinstall : /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/MyPackage/genConf/MyPackage/$(confpy)
	@echo "Installing /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/MyPackage/genConf/MyPackage in /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/InstallArea/python" ; \
	 $(install_command) --exclude="*.py?" --exclude="__init__.py" --exclude="*.confdb" /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/MyPackage/genConf/MyPackage /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/InstallArea/python ; \

/export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/MyPackage/genConf/MyPackage/$(confpy) : $(conflib) /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/MyPackage/genConf/MyPackage
	$(genconf_silent) $(genconfig_cmd)   -o /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/MyPackage/genConf/MyPackage -p $(package) \
	  --configurable-module=GaudiKernel.Proxy \
	  --configurable-default-name=Configurable.DefaultName \
	  --configurable-algorithm=ConfigurableAlgorithm \
	  --configurable-algtool=ConfigurableAlgTool \
	  --configurable-auditor=ConfigurableAuditor \
          --configurable-service=ConfigurableService \
	  -i ../$(tag)/$(library_prefix)MyPackage.$(shlibsuffix)

/export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/MyPackage/genConf/MyPackage:
	@ if [ ! -d /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/MyPackage/genConf/MyPackage ] ; then mkdir -p /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/MyPackage/genConf/MyPackage ; fi ;

MyPackageConfclean :: MyPackageConfuninstall
	$(cleanup_silent) $(remove_command) /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/MyPackage/genConf/MyPackage/$(confpy) /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/MyPackage/genConf/MyPackage/$(confdb)

uninstall :: MyPackageConfuninstall

MyPackageConfuninstall ::
	@$(uninstall_command) /export/share/gauss/woodsn/Weitao_HGTD/HGTDAthena/InstallArea/python
libMyPackage_so_dependencies = ../x86_64-slc6-gcc49-opt/libMyPackage.so
#-- start of cleanup_header --------------

clean :: MyPackageConfclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(MyPackageConf.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

MyPackageConfclean ::
#-- end of cleanup_header ---------------
