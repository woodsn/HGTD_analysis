#!/bin/bash
k=0;
cat filelist/mu200.list |
while read row; do
 echo $row
# cp run/JobOptions.py run/JobOptions${k}.py
# sed -i "2c jps.AthenaCommonFlags.FilesInput  = [ \"${row}\" ]" run/JobOptions${k}.py
# echo cp mytuple.root /eos/user/w/weitao/HGTDntuple/mytuple${k}.root
 sed -i "8c cp\ mytuple.root\ /eos/user/w/weitao/HGTDntuple/mytuple${k}.root" lxplusbatchscript.csh
 sed -i "7c athena\ /afs/cern.ch/user/w/weitao/HGTDAthena/run/JobOptions${k}.py" lxplusbatchscript.csh
 bsub -o Job${k}.log -q 1nh < lxplusbatchscript.csh
 k=$[k+1]
done
