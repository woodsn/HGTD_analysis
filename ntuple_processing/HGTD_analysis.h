//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Jun 23 14:50:03 2017 by ROOT version 6.04/18
// from TTree myTree/myTree
// found on file: myoutput.root
//////////////////////////////////////////////////////////

#ifndef HGTD_analysis_h
#define HGTD_analysis_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"

class HGTD_analysis {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           RunNum;
   Int_t           EvtNum;
   Int_t           JetNum;
   Int_t           PrimVtx;
   Int_t           bcID;
   Double_t        ActInteract;
   Double_t        AvgInteract;
   vector<double>  *hit_x_vec;
   vector<double>  *hit_y_vec;
   vector<double>  *hit_s_vec;
   vector<double>  *hit_t_vec;
   vector<double>  *hit_e_vec;
   vector<double>  *jet_pt_vec;
   vector<double>  *jet_eta_vec;
   vector<double>  *jet_phi_vec;
   vector<double>  *jet_e_vec;
   vector<double>  *jet_m_vec;
   vector<double>  *truth_jet_pt_vec;
   vector<double>  *truth_jet_eta_vec;
   vector<double>  *truth_jet_phi_vec;
   vector<double>  *truth_jet_e_vec;
   vector<double>  *truth_jet_m_vec;
   Int_t           Vtx_ntrack;
   Double_t        Vtx_z;

   // List of branches
   TBranch        *b_RunNum;   //!
   TBranch        *b_EvtNum;   //!
   TBranch        *b_JetNum;   //!
   TBranch        *b_PrimVtx;   //!
   TBranch        *b_bcID;   //!
   TBranch        *b_ActInteract;   //!
   TBranch        *b_AvgInteract;   //!
   TBranch        *b_hit_x_vec;   //!
   TBranch        *b_hit_y_vec;   //!
   TBranch        *b_hit_s_vec;   //!
   TBranch        *b_hit_t_vec;   //!
   TBranch        *b_hit_e_vec;   //!
   TBranch        *b_jet_pt_vec;   //!
   TBranch        *b_jet_eta_vec;   //!
   TBranch        *b_jet_phi_vec;   //!
   TBranch        *b_jet_e_vec;   //!
   TBranch        *b_jet_m_vec;   //!
   TBranch        *b_truth_jet_pt_vec;   //!
   TBranch        *b_truth_jet_eta_vec;   //!
   TBranch        *b_truth_jet_phi_vec;   //!
   TBranch        *b_truth_jet_e_vec;   //!
   TBranch        *b_truth_jet_m_vec;   //!
   TBranch        *b_Vtx_ntrack;   //!
   TBranch        *b_Vtx_z;   //!

   HGTD_analysis(TTree *tree=0);
   virtual ~HGTD_analysis();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef HGTD_analysis_cxx
HGTD_analysis::HGTD_analysis(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("myoutput.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("myoutput.root");
      }
      f->GetObject("myTree",tree);

   }
   Init(tree);
}

HGTD_analysis::~HGTD_analysis()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t HGTD_analysis::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t HGTD_analysis::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void HGTD_analysis::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   hit_x_vec = 0;
   hit_y_vec = 0;
   hit_s_vec = 0;
   hit_t_vec = 0;
   hit_e_vec = 0;
   jet_pt_vec = 0;
   jet_eta_vec = 0;
   jet_phi_vec = 0;
   jet_e_vec = 0;
   jet_m_vec = 0;
   truth_jet_pt_vec = 0;
   truth_jet_eta_vec = 0;
   truth_jet_phi_vec = 0;
   truth_jet_e_vec = 0;
   truth_jet_m_vec = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("RunNum", &RunNum, &b_RunNum);
   fChain->SetBranchAddress("EvtNum", &EvtNum, &b_EvtNum);
   fChain->SetBranchAddress("JetNum", &JetNum, &b_JetNum);
   fChain->SetBranchAddress("PrimVtx", &PrimVtx, &b_PrimVtx);
   fChain->SetBranchAddress("bcID", &bcID, &b_bcID);
   fChain->SetBranchAddress("ActInteract", &ActInteract, &b_ActInteract);
   fChain->SetBranchAddress("AvgInteract", &AvgInteract, &b_AvgInteract);
   fChain->SetBranchAddress("hit_x_vec", &hit_x_vec, &b_hit_x_vec);
   fChain->SetBranchAddress("hit_y_vec", &hit_y_vec, &b_hit_y_vec);
   fChain->SetBranchAddress("hit_s_vec", &hit_s_vec, &b_hit_s_vec);
   fChain->SetBranchAddress("hit_t_vec", &hit_t_vec, &b_hit_t_vec);
   fChain->SetBranchAddress("hit_e_vec", &hit_e_vec, &b_hit_e_vec);
   fChain->SetBranchAddress("jet_pt_vec", &jet_pt_vec, &b_jet_pt_vec);
   fChain->SetBranchAddress("jet_eta_vec", &jet_eta_vec, &b_jet_eta_vec);
   fChain->SetBranchAddress("jet_phi_vec", &jet_phi_vec, &b_jet_phi_vec);
   fChain->SetBranchAddress("jet_e_vec", &jet_e_vec, &b_jet_e_vec);
   fChain->SetBranchAddress("jet_m_vec", &jet_m_vec, &b_jet_m_vec);
   fChain->SetBranchAddress("truth_jet_pt_vec", &truth_jet_pt_vec, &b_truth_jet_pt_vec);
   fChain->SetBranchAddress("truth_jet_eta_vec", &truth_jet_eta_vec, &b_truth_jet_eta_vec);
   fChain->SetBranchAddress("truth_jet_phi_vec", &truth_jet_phi_vec, &b_truth_jet_phi_vec);
   fChain->SetBranchAddress("truth_jet_e_vec", &truth_jet_e_vec, &b_truth_jet_e_vec);
   fChain->SetBranchAddress("truth_jet_m_vec", &truth_jet_m_vec, &b_truth_jet_m_vec);
   fChain->SetBranchAddress("Vtx_ntrack", &Vtx_ntrack, &b_Vtx_ntrack);
   fChain->SetBranchAddress("Vtx_z", &Vtx_z, &b_Vtx_z);
   Notify();
}

Bool_t HGTD_analysis::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void HGTD_analysis::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t HGTD_analysis::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef HGTD_analysis_cxx
